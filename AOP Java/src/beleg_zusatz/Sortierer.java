package beleg_zusatz;

import java.io.*;

public class Sortierer {

	public static Artikel artikel[];
	
	// Tauscht 2 Elemente im Feld
	public static void swapArtikel(int i, int j) {
		Artikel temp = artikel[i];
		artikel[i] = artikel[j];
		artikel[j] = temp;
	}
	
	public static void main(String[] args) {
		String dateiName = args.length > 2 ? args[1] : "artikel.dat";
		
		// Datei lesen
		try {		
			File file = new File(dateiName);
			if ( file.length()%125 != 0 ) // 124 Artikel + \n
				throw new Exception( "Ungültiger Rest: " + file.length()%125 );
			artikel = new Artikel[(int) (file.length()/125)];
			
			int i = 0;
			String zeile;
			BufferedReader in = new BufferedReader( new FileReader(dateiName) );
			while ( (zeile = in.readLine()) != null )
				artikel[i++] = new Artikel(zeile);
			in.close(); 
		} catch (Exception e) {
			System.out.println( "Fehler beim Lesen der Datei!!! Meldung: " + e.getMessage() );
		}
					
		// Artikel trennen nach Artikelnummer % 10 > 3
		int firstBigger = 0;
		for (int j = 0; j < artikel.length; j++)
			if ( artikel[j].LastDigitAN() > 3 && firstBigger != j)
				swapArtikel(firstBigger++,j);
				
		int numSorted = (firstBigger == -1) ? 0 : firstBigger;
		
		// Aufsteigend sortieren der gesuchten Elemente (Kleinstes Element zuerst)
		for (int i = 0; i < numSorted; i++) {
			int min = i;
			for (int j = i; j < numSorted; j++)
				if ( artikel[j].Minimalbestand < artikel[min].Minimalbestand)  
					min = j;

			swapArtikel(min,i);
		}

		// Ausgabe
		System.out.println( "Minimalbestände		Artikelnummer" );
		for (int i = 0; i < artikel.length; i++)
			System.out.println( artikel[i].Ausgabe() );
		System.out.println( "\n	->" + firstBigger + " Elemente sortiert!" );
	}
}
