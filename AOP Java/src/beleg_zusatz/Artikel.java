package beleg_zusatz;

public class Artikel {
	public int Artikelnummer;			// 8
	public String Artikelbezeichnung;	// 30
	public String Mengeneinheit;		// 2
	public short Bestand;				// 4
	public short Maximalbestand;		// 4
	public short Minimalbestand;		// 3
	public String Hersteller;			// 41
	public float Preis;					// 7
	public float Maximalpreis;			// 7
	public float Minimalpreis;			// 7
	public byte Lieferzeit;				// 1
	
	Artikel( String zeile ) throws Exception  {
		if (zeile.length() != 124 ) // Falsche L�nge -> Sofort Abbruch
			throw new Exception("Zeilenl�nge falsch! (" + zeile.length() + " Zeichen)" );
		
		// Zeile in einzelne Daten wandeln
		
		Artikelnummer = Integer.valueOf( zeile.substring(0, 8) ).intValue();
		Artikelbezeichnung = zeile.substring(9, 39).trim();
		Mengeneinheit = zeile.substring(40, 42).trim();
		Bestand = Short.valueOf( zeile.substring(43, 47) ).shortValue();
		Maximalbestand = Short.valueOf( zeile.substring(48, 52) ).shortValue();
		Minimalbestand = Short.valueOf( zeile.substring(53, 56) ).shortValue();
		Hersteller = zeile.substring(57, 98).trim();
		Preis = Float.valueOf( zeile.substring(99, 106) ).floatValue();
		Maximalpreis = Float.valueOf( zeile.substring(107, 114 ) ).floatValue();
		Minimalpreis = Float.valueOf( zeile.substring(115, 122) ).floatValue();
		Lieferzeit = Byte.valueOf( zeile.substring(123, 124) ).byteValue();
	}
	
	public String Ausgabe() {
		String ausgabe = Minimalbestand + "			" + Artikelnummer; 
		return ausgabe;
	}
	
	
	// Convenience...
	public int LastDigitAN() {
		return Artikelnummer % 10;
	}
	
	// Nur zum debuggen...
	public String DebugAusgabe() {
		String ausgabe = Artikelnummer + "," +
						Artikelbezeichnung + "," +
						Mengeneinheit + "," +
						Bestand + "," +
						Maximalbestand + "," +
						Minimalbestand + "," +
						Hersteller + "," +
						Preis + "," +
						Maximalpreis + "," +
						Minimalpreis + "," +
						Lieferzeit; 
		return ausgabe;
	}
}
