package belegvier;

public interface BigNatural {
	public void setValue(int wert) throws RuntimeException;
	//Wert wird gesetzt; beiWerten < 0 : RuntimeException
	
	public void setValue( BigNatural wert );
	//Wert wird gesetzt
	
	public int toInteger() throws RuntimeException;
	//gespeicherterWertwirdalsIntegerzurueckgegeben;
	//Fallsdiesnichtmoeglichist:RuntimeException
	
	public BigNatural add( BigNatural wert ) throws RuntimeException;
	//EinneuesObjektwirderzeugtundmitderSummezurueckgegeben;
	//FallsderWertzugrosswird:RuntimeException
	
	public BigNatural subtract( BigNatural wert ) throws RuntimeException;
	//EinneuesObjektwirderzeugtundmitderDifferenzzurueckgegeben
	//FallsderWertvonwertgroesseralsvonthisist:RuntimeException
	
	public BigNatural multiply( BigNatural wert ) throws RuntimeException;
	//EinneuesObjektwirderzeugtundmitdemProduktzurueckgegeben
	//FallsderWertzugrosswird:RuntimeException
	
	public BigNatural divide( BigNatural wert ) throws RuntimeException;
	//EinneuesObjektwirderzeugtundmitdemTeilerzurueckgegeben
	//Fallswert=0:RuntimeException
	
	public BigNatural remainder( BigNatural wert ) throws RuntimeException;
	//EinneuesObjektwirderzeugtundmitdemRestderDivision
	//durchwertzurueckgegeben
	//Fallswert=0:RuntimeException

	public boolean isPrime();
	//Genaudann,wenndergespeicherteWerteeinePrimzahlist,wird
	//truezurueckgegeben}
}
