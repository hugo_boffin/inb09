package belegvier;

/*
 * 
 * AOP Java Beleg 4
 * 
 * Christian Petzold (09INB2)
 * 
 * */

public class BigNat_cpetzold implements BigNatural {

	// Zahl zur Basis 10, Jedes Array Element ist eine Ziffer (b=10)
	// Index 0: 1. Ziffer von rechts (Exponent 10^0), usw.
	private short[] ziffer;

	public BigNat_cpetzold() {
		ziffer = new short[25];
		Nullify();
	}

	public BigNatural add(BigNatural summand) throws RuntimeException {
		BigNat_cpetzold max = new BigNat_cpetzold();
		max.Maxify();
		max.subtract(summand); // Summe > Maximum?

		if (greaterThan(max)) // Ueberlauf?
			throw new RuntimeException();

		BigNat_cpetzold summe = new BigNat_cpetzold();

		short[] hilf = ((BigNat_cpetzold) summand).ziffer;
		short uebertrag = 0;
		for (int i = 0; i < ziffer.length; i++) {
			summe.ziffer[i] = (short) (ziffer[i] + (hilf[i] + uebertrag));
			uebertrag = (short) (summe.ziffer[i] / 10);
			summe.ziffer[i] %= 10;
		}
		// Der letzte Uebertrag wird "geschluckt"

		return summe;
	}

	public BigNatural divide(BigNatural divisor) throws RuntimeException {
		if (((BigNat_cpetzold) divisor).equals(new BigNat_cpetzold())) // Division
			// durch
			// 0?
			throw new RuntimeException();

		BigNat_cpetzold quotient = new BigNat_cpetzold();

		/*
		 * // Naiver Ansatz BigNat_cpetzold eins = new BigNat_cpetzold();
		 * eins.setValue(1); BigNat_cpetzold rest = new BigNat_cpetzold();
		 * rest.setValue(this); while ( rest.greaterThanOrEqual(
		 * (BigNat_cpetzold)divisor ) ) { rest = (BigNat_cpetzold)rest.subtract(
		 * (BigNat_cpetzold)divisor ); quotient =
		 * (BigNat_cpetzold)quotient.add(eins); }
		 */

		int pos = 0;
		int offset = leadingDigit() + 1; // Fuehrende Nullen direkt weg...

		BigNat_cpetzold temp = new BigNat_cpetzold();
		while (offset != 0) { // Solange bis Zahl voll verarbeitet
			boolean stillGreater = true;
			while (stillGreater && offset != 0) {
				// Links shiften, neue Ziffer "runterziehen"
				for (int i = ziffer.length - 1; i > 0; i--)
					temp.ziffer[i] = temp.ziffer[i - 1];
				temp.ziffer[0] = ziffer[--offset];

				// Durch Divisor teilbar? Ansonsten weiter
				stillGreater = ((BigNat_cpetzold) divisor).greaterThan(temp);
				if (stillGreater)
					quotient.ziffer[pos++] = (short) 0;
			}
			while (temp.greaterThanOrEqual((BigNat_cpetzold) divisor)) {
				quotient.ziffer[pos]++;
				temp = (BigNat_cpetzold) temp.subtract(divisor);
			}
			if (quotient.ziffer[pos] != 0)
				pos++;
		}
		for (int i = 0; i < (pos) / 2; i++) {
			short hilf = quotient.ziffer[i];
			quotient.ziffer[i] = quotient.ziffer[pos - i - 1];
			quotient.ziffer[pos - i - 1] = hilf;
		}

		return quotient;
	}

	// this == zahl ??
	private boolean equals(BigNat_cpetzold zahl) {
		for (int i = 0; i < ziffer.length; i++)
			if (ziffer[i] != zahl.ziffer[i])
				return false;
		return true;
	}

	// this > zahl ??
	private boolean greaterThan(BigNat_cpetzold zahl) {
		for (int i = ziffer.length - 1; i >= 0; i--) {
			// Sucht die hoechstwertigste ungleiche Stelle
			if (ziffer[i] > zahl.ziffer[i])
				return true;
			if (ziffer[i] < zahl.ziffer[i])
				return false;
		}
		return false;
	}

	// this >= zahl ??
	private boolean greaterThanOrEqual(BigNat_cpetzold zahl) {
		// Sucht die hoechstwertigste ungleiche Stelle 
		for (int i = ziffer.length - 1; i >= 0; i--) {
			if (ziffer[i] > zahl.ziffer[i])
				return true;
			if (ziffer[i] < zahl.ziffer[i])
				return false;
		}
		return true;
	}

	public boolean isPrime() {
		BigNat_cpetzold zero = new BigNat_cpetzold();
		zero.setValue(0);
		BigNat_cpetzold eins = new BigNat_cpetzold();
		eins.setValue(1);
		BigNat_cpetzold i = new BigNat_cpetzold();
		i.setValue(2);
		BigNat_cpetzold vier = new BigNat_cpetzold();
		i.setValue(4);

		if (equals(zero) || equals(eins))
			return false;

		BigNat_cpetzold n = new BigNat_cpetzold();
		n.setValue(this);
		
		// Wurzelfunktion existiert nicht, aber fuer x>=4
		// ist y=f(x)=x/2 effizienter und immer echt
		// groesser als Wurzel x
		if ( n.greaterThan(vier) ) 
			n.divide(i);
		else
			n.subtract(eins);

		while (n.greaterThan(i)) {
			if (((BigNat_cpetzold) remainder(i)).equals(zero))
				return false;
			i = (BigNat_cpetzold) i.add(eins);
		}
		return true;
	}

	// Fuehrende Ziffer != 0, bzw 0 selbst
	private int leadingDigit() {
		int links = ziffer.length;
		while (links != 0 && ziffer[--links] == 0)
			;
		return links;
	}

	// Maximal darstellbare Zahl
	//10^20 = 100000000000000000000
	private void Maxify() {
		//for (int i = 0; i < ziffer.length; i++)
		//	ziffer[i] = 9;
		Nullify();
		ziffer[20] = 1;
	}

	public BigNatural multiply(BigNatural faktor) throws RuntimeException {
		BigNat_cpetzold max = new BigNat_cpetzold();
		max.Maxify();
		max.divide(faktor);

		if (greaterThan((BigNat_cpetzold) max))
			throw new RuntimeException();

		BigNat_cpetzold produkt = new BigNat_cpetzold();
		short[] hilf = ((BigNat_cpetzold) faktor).ziffer;
		for (int j = 0; j < ziffer.length; j++) {
			BigNat_cpetzold temp = new BigNat_cpetzold();
			short uebertrag = 0;
			for (int i = 0; i < ziffer.length - j; i++) {
				temp.ziffer[i + j] = (short) (ziffer[i] * hilf[j] + uebertrag);
				uebertrag = (short) (temp.ziffer[i + j] / 10);
				temp.ziffer[i + j] %= 10;
			}
			produkt = (BigNat_cpetzold) produkt.add(temp);
		}
		return produkt;
	}

	// Convenience Funktion fuer die 0
	private void Nullify() {
		for (int i = 0; i < ziffer.length; i++)
			ziffer[i] = 0;
	}

	public BigNatural remainder(BigNatural divisor) throws RuntimeException {
		// Division durch 0?
		if (((BigNat_cpetzold) divisor).equals(new BigNat_cpetzold())) 
			throw new RuntimeException();

		int offset = leadingDigit() + 1; // Fuehrende Nullen direkt weg...

		BigNat_cpetzold temp = new BigNat_cpetzold();
		while (offset != 0) { // Solange bis Zahl voll verarbeitet
			boolean stillGreater = true;
			while (stillGreater && offset != 0) {
				// Links shiften, neue Ziffer "runterziehen"
				for (int i = ziffer.length - 1; i > 0; i--)
					temp.ziffer[i] = temp.ziffer[i - 1];
				temp.ziffer[0] = ziffer[--offset];

				// Durch Divisor teilbar? Ansonsten weiter
				stillGreater = ((BigNat_cpetzold) divisor).greaterThan(temp);
			}
			while (temp.greaterThanOrEqual((BigNat_cpetzold) divisor))
				temp = (BigNat_cpetzold) temp.subtract(divisor);
		}

		return temp;
	}
	
	public void setValue(BigNatural wert) {
		short[] hilf = ((BigNat_cpetzold) wert).ziffer;
		for (int i = 0; i < ziffer.length; i++)
			ziffer[i] = hilf[i];
	}

	public void setValue(int wert) throws RuntimeException {
		if (wert < 0)
			throw new RuntimeException();

		Nullify();
		// Zahl in meine Array-Struktur zur Basis 10 bringen
		int i = 0;
		while (wert != 0) {
			ziffer[i++] = (short) (wert % 10);
			wert /= 10;
		}
	}

	public BigNatural subtract(BigNatural subtrahend) throws RuntimeException {
		if (((BigNat_cpetzold) subtrahend).greaterThan(this))
			throw new RuntimeException();

		BigNat_cpetzold differenz = new BigNat_cpetzold();

		short[] hilf = ((BigNat_cpetzold) subtrahend).ziffer;
		short uebertrag = 0;
		for (int i = 0; i < ziffer.length; i++) {
			differenz.ziffer[i] = (short) (ziffer[i] - (hilf[i] + uebertrag));
			if (differenz.ziffer[i] < 0) {
				uebertrag = 1;
				differenz.ziffer[i] += 10;
			} else
				uebertrag = 0;
		}
		// Der letzte Uebertrag wird "geschluckt"

		return differenz;
	}

	public int toInteger() throws RuntimeException {
		BigNat_cpetzold max = new BigNat_cpetzold();
		max.setValue(Integer.MAX_VALUE);
		if (greaterThan(max))
			throw new RuntimeException();

		// Maximaler Integer-Wert haette 10 Stellen, also reicht es soweit
		// aufzuaddieren
		int summe = 0;
		int exp = 1;
		for (int i = 0; i < 10; i++) {
			summe += ziffer[i] * exp;
			exp *= 10;
		}
		return (int) summe;
	}

	// Behilfsfunktion wenn toInteger versagt
	public String toString() {
		int ende = leadingDigit();

		String zahl = new String();
		for (int i = ende; i >= 0; i--)
			zahl += Short.toString(ziffer[i]);

		return zahl;
	}
}
