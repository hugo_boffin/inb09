package belegdrei;

public class Test01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

		KubFunktion f = new Kub_cpetzold();
		f.init(1.0, 5.0, 1.0, -5.0); // x^3 + 5x^2 + x - 5 // 3 Nullstellen
		//f.init(0.0, -7.0, 5.0, 18.0);
		//f.init(0.0, 0.0, 25.0, 17.0);
		//f.init(0.0, 0.0, 0, 0);
		
		//f.init(0.0, 1.0, 2.0, 1.0);
		int nullstellen = f.berechneNullstellen(-20.0, 10.0);
		
		System.out.println(nullstellen);
		for (int i = 1; i <= nullstellen; i++)
			System.out.println("x0_"+i+" = "+f.nullstelleNr(i));
			
		double nullst = f.nullstelleNr(1);
		KubFunktion g = f.differenziere();
		double y = g.funktionswert(5.0);
		System.out.println("f'(5) = "+y);
		System.out.println("xn = "+nullst);
	}
}
