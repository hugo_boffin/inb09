package belegdrei;

public class Kub_cpetzold implements KubFunktion {
	public Kub_cpetzold() {
		init(0, 0, 0, 0); // Zur Sicherheit
	}
	public void init(double a, double b, double c, double d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;

		anzahl_nullstellen = 0;
		n = new double[3];
	}

	public double funktionswert(double x) {
		return a * Math.pow(x, 3) + b * Math.pow(x, 2) + c * x + d;
	}

	public int berechneNullstellen(double xunten, double xoben) {
		anzahl_nullstellen = 0;
		if (a == 0.0 && b == 0.0 && c == 0.0) {
			// Konstante Funktion

			if (d == 0.0)
				anzahl_nullstellen = -1;

		} else if (a == 0.0 && b == 0.0) {
			// Gerade	~	y = bx + a -> (y-a)/b = x
			neueNullstelle ( (d != 0.0) ? ((-d) / c) : 0.0 );
		} else if (a == 0.0) {
			// Polynom 2. Grades
			if (c * c >= 4 * b * d) { // Wurzel -> (0,inf)
				neueNullstelle((-c + Math.sqrt(c * c - 4 * b * d)) / (2 * b));
				neueNullstelle((-c - Math.sqrt(c * c - 4 * b * d)) / (2 * b));
			}
		} else {
			// Polynom 3. Grades	~	Intervallschachtelung

			// 1. Ableitung und Extrema, geclamped in Suchintervall, e1 <= e2
			KubFunktion ff = differenziere();
			ff.berechneNullstellen(xunten, xoben);
			double e1 = Math.min(xoben, Math.max(xunten, Math.min(ff.nullstelleNr(1), ff.nullstelleNr(2))));
			double e2 = Math.min(xoben, Math.max(xunten, Math.max(ff.nullstelleNr(1), ff.nullstelleNr(2))));

			// xunten <= e1 <= e2 <= xoben
			sucheNullstelle(e2, xoben);
			sucheNullstelle(xunten, e1);
			sucheNullstelle(e1, e2);
		}

		return anzahl_nullstellen;
	}

	public double nullstelleNr(int i) {
		if (i > 0 && i <= anzahl_nullstellen)
			return n[i-1];
		else
			return 0.0;
	}

	public KubFunktion differenziere() {
		Kub_cpetzold g = new Kub_cpetzold();
		g.init(0.0, a * 3.0, b * 2.0, c);
		return g;
	}

	public KubFunktion addiere(KubFunktion f) {
		Kub_cpetzold s = (Kub_cpetzold) f;
		Kub_cpetzold h = new Kub_cpetzold();
		h.init(a + s.a, b + s.b, c + s.c, d + s.d);
		return h;
	}
	
	// Eigene Private Funktionen

	private void sucheNullstelle(double a, double b) {
		if (a == b) {
			if (funktionswert(a) == 0)
				neueNullstelle(a);
			return;
		}
		while (Math.abs(a - b) > 0.0005) {
			double m = 0.5 * (a + b);
			if (Math.signum(funktionswert(a)) != Math.signum(funktionswert(m)))
				b = m;
			else if (Math.signum(funktionswert(b)) != Math.signum(funktionswert(m)))
				a = m;
			else
				return;
		}
		neueNullstelle(0.5 * (a + b));
	}

	private boolean neueNullstelle(double x0) { // Filtert doppelt Nullstellen
		for (int i = 0; i < anzahl_nullstellen; i++)
			if (Math.abs(n[i] - x0) < 0.0005) //Nullstelle schon vorhanden? (Leichte Toleranz)
				return false;

		n[anzahl_nullstellen++] = x0;
		java.util.Arrays.sort( n, 0, anzahl_nullstellen); // Aufsteigend sortieren
		
		return true;
	}

	private double a, b, c, d;
	private double[] n;
	private int anzahl_nullstellen;
}
