package uebung05;

import Prog1Tools.IOTools;

public class MessreiheMain {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Messreihe mr = new Messreihe();
		
		do {
			 mr.NeuerMesswert( IOTools.readDouble("Messwert? ") );
			 System.out.println( "Erwartungswert: " + mr.getErwartungswert() );
			 System.out.println( "Standardabweichung: " + mr.getStandartabweichung() );
		} while ( IOTools.readBoolean("Noch einer? "));
		
		System.out.println( "---------" );
		System.out.println( "Erwartungswert: " + mr.getErwartungswert() );
		System.out.println( "Standardabweichung: " + mr.getStandartabweichung() );
	}
}
