package uebung05;

public class Zylinder {
	Zylinder( double radius, double hoehe ) {
		this.radius = radius;
		this.hoehe = hoehe;
	}

	public void SetRadius( double radius ) {
		this.radius = radius;
	}
	
	public void SetHoehe( double hoehe ) {
		this.hoehe = hoehe;
	}
	
	public double GetRadius( ) {
		return radius;
	}
	
	public double GetHoehe( ) {
		return hoehe;
	}
	
	public double GetVolumen() { // V = Pi*R^2 * h
		return Math.PI * Math.pow(radius,2) * hoehe;
	}
	
	public double GetOberflaeche() { // Ao = 2*Pi*r*(r*h)
		return 2 * Math.PI * radius * (radius + hoehe);
	}

	private double radius;
	private double hoehe;
}
