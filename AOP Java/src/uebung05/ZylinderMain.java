package uebung05;

import java.text.DecimalFormat;
//import Prog1Tools.IOTools;

public class ZylinderMain {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Zylinder z = new Zylinder( IOTools.readDouble("Radius? "), IOTools.readDouble("Hoehe? ") );
		Zylinder z = new Zylinder( 4.0, 7.0 );

		zylinderAusgabe(z);
		
		z.SetRadius(5.0);
		z.SetHoehe(3.0);

		zylinderAusgabe(z);
	}
	
	public static void zylinderAusgabe( Zylinder z ) {
		DecimalFormat df = new DecimalFormat("0.000"); // Auf 3 Nachkommastellen runden
		
		System.out.println("Zylinder - Radius: " + z.GetRadius() + ", H�he: " + z.GetHoehe() );
		System.out.println("Oberflaeche: " + df.format( z.GetOberflaeche() ) );
		System.out.println("Volumen: " + df.format( z.GetVolumen() ) );
		System.out.println();
	}
}
