package uebung05;

public class Messreihe {
	private int n;
	private double e;
	private double eq;
	
	Messreihe() {
		n = 0;
		e = 0;
		eq = 0;
	}
	
	public void NeuerMesswert( double wert ) {
		n++;
		e += wert;
		eq += (wert*wert);
	}
	
	public double getErwartungswert() {
		return e / n;
	}
	
	public double getStandartabweichung() {
		return Math.sqrt( (eq/n) - (e/n)*(e/n) );
	}
}
