package SiebzehnUndVier;

import java.io.Serializable;

/**
 * Siebzehn und Vier in Java
 * ---
 * Die Klasse "Spielkarte" beschreibt wie der Name schon sagt
 * eine Karte im Spiel. Es wird Die Augenzahl, Farbe und das
 * Symbol gespeichert.
 * 
 * @author	Christian Petzold
 * 			Seminargruppe 09INB02
 * 			Matrikelnummer 50467
 * @version	1.0
 * 
 * Quellen:
 * http://de.wikipedia.org/wiki/Siebzehn_und_vier
 * http://de.wikipedia.org/wiki/Skat#Skatblatt
 * 
 * Deutsches Skatblatt, 32 Karten
 * 
 * Farben: Eichel, Gr�n(Blatt), Rot(Herz), Schellen
 * 
 * Karte	Symbol		Augen
 * --------------------------
 * Ass		A			11
 * K�nig	K			4
 * Ober		O			3
 * Unter	U			2
 * 
 * Zehn		10			10
 * Neun		9			9!
 * Acht		8			8!
 * Sieben	7			7!
 * 
 * ACHTUNG: Zahlenkarten - Augen entsprechend ihres Wertes,
 * *nicht* wie beim Skat 7,8,9 := 0
 * 
 */

@SuppressWarnings("serial")
public class Spielkarte implements Serializable {
	enum Farbe {
		Eichel, Gruen, Rot, Schellen
	}

	public Spielkarte(int Augen, Farbe farbe, String Symbol) {
		setAugen(Augen);
		setFarbe(farbe);
		setSymbol(Symbol);
	}

	public int getAugen() {
		return augen;
	}

	public void setAugen(int augen) {
		this.augen = augen;
	}

	public Farbe getFarbe() {
		return farbe;
	}

	public void setFarbe(Farbe farbe) {
		this.farbe = farbe;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String toString() {
		return farbe.toString() + " " + symbol;
	}

	private int augen;
	private Farbe farbe;
	private String symbol;
}
