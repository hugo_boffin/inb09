package SiebzehnUndVier;

/**
 * Siebzehn und Vier in Java
 * ---
 * Die Klasse "Spieler" implementiert die Kartenhand
 * eines Spielteilnehmers. Au�erdem werden seine Siege
 * festgehalten.
 * 
 * @author	Christian Petzold
 * 			Seminargruppe 09INB02
 * 			Matrikelnummer 50467
 * @version	1.0
 */
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Stack;

@SuppressWarnings("serial")
public class Spieler implements Serializable {
	public Collection<Spielkarte> hand;
	private int siege;
	private String name;

	public Spieler(String name) {
		hand = new Stack<Spielkarte>();
		this.name = name;
	}

	public int augenSumme() {
		int summe = 0;
		for (Iterator<Spielkarte> iterator = hand.iterator(); iterator.hasNext();)
			summe += iterator.next().getAugen();
		return summe;
	}

	public void zurueckInStapel(Collection<Spielkarte> stapel) {
		stapel.addAll(hand);
		hand.clear();
	}
	
	public int getSiege() {
		return siege;
	}

	public void setSiege(int siege) {
		this.siege = siege;
	}

	public void gewonnen() {
		siege++;
	}

	public void neueKarte(Spielkarte neu) {
		hand.add(neu);
	}
	public Iterator<Spielkarte> getHandIterator() {
		return hand.iterator();
	}

	public int anzahlKarten() {
		return hand.size();
	}
	
	public String toString() {
		return name;
	}
}
