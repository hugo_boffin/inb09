package SiebzehnUndVier;

/**
 * Siebzehn und Vier in Java
 * ---
 * Die Klasse "Spiel" k�mmert sich um den gesamten
 * Spielablauf, Spieler, Karten, Mischen, Geben, usw.
 * 
 * @author	Christian Petzold
 * 			Seminargruppe 09INB02
 * 			Matrikelnummer 50467
 * @version	1.0
 */
import java.io.Serializable;
import java.util.Collections;
import java.util.Stack;

@SuppressWarnings("serial")
public class Spiel implements Serializable {
	private Stack<Spielkarte> kartenStapel = null;
	private Spieler geber = new Spieler("Geber");
	private Spieler spieler = new Spieler("Spieler");
	private Spieler lastGewinner = null;
	private boolean amSpielen = false;
	
	public Spiel() {
		blattErstellen();
		neuesSpiel();
	}
	
	/**
	 * Neue Spiel-Serie beginnen (Punkte auf 0 setzen)
	 */
	public void neuesSpiel() {
		amSpielen = false;
		spieler.setSiege(0);
		geber.setSiege(0);
		naechsteRunde();

		amSpielen = true;
	}

	/**
	 * N�chstes Spiel (in der Serie)
	 */
	public void naechsteRunde() {
		if (!amSpielen) {
			spieler.zurueckInStapel(kartenStapel);
			geber.zurueckInStapel(kartenStapel);

			kartenMischen();

			amSpielen = true;

			gibKarte(spieler);
			gibKarte(spieler);
			
			gibKarte(geber);
		}
	}
	
	/**
	 * Der Spieler m�chte noch eine Karte
	 */
	public void nimmKarte() {
		if (amSpielen) {
			gibKarte(spieler);
			if ( !ueberreizt(spieler) )
				geberAktion();
		}
	}

	/**
	 * Der Spieler m�chte keine Karte mehr
	 */
	public void genugKarten() {
		if (amSpielen) {
			geberAktion();
			
			if ( !ueberreizt(geber) )
				if (spieler.augenSumme() > geber.augenSumme())
					spielEnde(spieler);
				else
					spielEnde(geber);
		}
	}
	
	private void geberAktion() {
		/*
		 *  Geber nimmt Karte sofern er unter 13 Augen ist
		 *  Geringes Risiko das 10 oder 11 �berreizt
		 *  (bzw. muss die 2. Karte genommen werden)
		 */
		
		if (geber.augenSumme() < 13 || geber.anzahlKarten() < 2)
			gibKarte(geber);
	}
	
	public boolean isAmSpielen() {
		return amSpielen;
	}

	public Spieler getGeber() {
		return geber;
	}
	
	public Spieler getSpieler() {
		return spieler;
	}
	
	public Spieler getLastGewinner() {
		return lastGewinner;
	}
		
	/**
	 * Kartensatz erstellen (Skatblatt 32 Karten)
	 */
	private void blattErstellen() {
		kartenStapel = new Stack<Spielkarte>();
		for (Spielkarte.Farbe farbe : Spielkarte.Farbe.values()) {
			kartenStapel.push(new Spielkarte(11, farbe, "A"));
			kartenStapel.push(new Spielkarte(4, farbe, "K"));
			kartenStapel.push(new Spielkarte(3, farbe, "O"));
			kartenStapel.push(new Spielkarte(2, farbe, "U"));

			kartenStapel.push(new Spielkarte(10, farbe, "10"));
			kartenStapel.push(new Spielkarte(9, farbe, "9"));
			kartenStapel.push(new Spielkarte(8, farbe, "8"));
			kartenStapel.push(new Spielkarte(7, farbe, "7"));
		}
	}
	
	private void kartenMischen() {
		Collections.shuffle(kartenStapel); // Shuffle mischt Collection gleichverteilt
	}
	
	private void gibKarte(Spieler person) {
		if (amSpielen) {
			person.neueKarte( kartenStapel.pop() );
			if ( ueberreizt(person) )
				spielEnde( (person == spieler) ? geber : spieler);
		}		
	}
	
	private boolean ueberreizt(Spieler person) {
		return (person.augenSumme()>21 && person.anzahlKarten()>2); // Ausnahme der 21 Augen Regel: 2 Asse a 11
	}

	private void spielEnde( Spieler gewinner ) {
		if (amSpielen) {
			gewinner.gewonnen();
			lastGewinner = gewinner;
			amSpielen = false;
		}
	}
}
