package SiebzehnUndVier;

/**
 * Siebzehn und Vier in Java
 * ---
 * Die Klasse "TischPanel" implementiert ein JPanel das
 * den Spieltisch mitsamt aller Karten zeichnet.
 * 
 * @author	Christian Petzold
 * 			Seminargruppe 09INB02
 * 			Matrikelnummer 50467
 * @version	1.0
 */
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.Iterator;

@SuppressWarnings("serial")
public class TischPanel extends JPanel {
	private Font bigFont = new Font( "Verdana", Font.PLAIN, 20 );
	private Spiel spiel = null;
	
	public TischPanel(Spiel spiel) {
		this.spiel = spiel;

		setBackground( new Color(0, 120, 0) );
	}
	
	/**
	 * Zeichenroutine überschreiben
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// Karten
		zeichneHand(g, spiel.getGeber(), 10, 10, spiel.isAmSpielen());
		zeichneHand(g, spiel.getSpieler(), 10, getSize().height - 165, false);

		// Spielstand
		String stand = "Spieler " + spiel.getSpieler().getSiege() + " : " + spiel.getGeber().getSiege() + " Geber";
		FontMetrics fm = g.getFontMetrics();
		g.setColor(Color.WHITE);
		g.drawString(stand, getSize().width / 2 - fm.stringWidth(stand) / 2, getSize().height / 2);
	}
	
	private void zeichneHand(Graphics g, Spieler spieler, int x, int y, boolean verdeckt) {
		int offset = 0;
		Font font = g.getFont();
		int h = g.getFontMetrics(font).getHeight();

		// Augensumme
		g.setColor(Color.WHITE);
		if (verdeckt)
			g.drawString(spieler.toString(), x, 10 + y);
		else
			g.drawString(spieler + " - Augen: " + spieler.augenSumme(), x, 10 + y);

		g.setFont(bigFont);
		// Jede Karte zeichnen
		for (Iterator<Spielkarte> iterator = spieler.getHandIterator(); iterator.hasNext();) {
			Spielkarte karte = iterator.next();

			if (verdeckt) {
				// Rückseite
				g.setColor( new Color( 0,150, 250) );
				g.fillRoundRect(x + offset, 20 + y, 90, 135, 20, 20);
				g.setColor(Color.BLACK);
				g.drawRoundRect(x + offset, 20 + y, 90, 135, 20, 20);
			} else {
				int w = g.getFontMetrics(bigFont).stringWidth( karte.getSymbol() );
				
				// Karte
				g.setColor(Color.WHITE);
				g.fillRoundRect(x + offset, 20 + y, 90, 135, 20, 20);
				g.setColor(Color.BLACK);
				g.drawRoundRect(x + offset, 20 + y, 90, 135, 20, 20);
	
				// Symbol
				g.setColor(Color.BLACK);
				g.drawString(karte.getSymbol(), x + offset + 10, 30 + y + h);
				g.drawString(karte.getSymbol(), x + offset + 80 - w, 145 + y);
				
				// Farbe
				switch (karte.getFarbe()) {
				case Eichel:
					g.setColor(new Color( 150, 75, 0));
					g.fillOval(x + offset + 10, 125 + y,20,20);
					g.fillOval(x + offset  + 60, 30 + y,20,20);
					break;
				case Gruen:
					g.setColor( new Color( 0, 200, 0) );
					g.fillOval(x + offset + 10, 125 + y,20,20);
					g.fillOval(x + offset  + 60, 30 + y,20,20);
					break;
				case Rot:
					g.setColor( Color.RED );
					g.fillOval(x + offset + 10, 125 + y,20,20);
					g.fillOval(x + offset  + 60, 30 + y,20,20);
					break;
				case Schellen:
					g.setColor(new Color( 230,230, 50));
					g.fillOval(x + offset + 10, 125 + y,20,20);
					g.fillOval(x + offset  + 60, 30 + y,20,20);
					break;
				}
			}
			offset += 110;
		}
		g.setFont(font);
	}
}
