package SiebzehnUndVier;

/**
 * Siebzehn und Vier in Java
 * ---
 * Die Klasse "SiebzehnUndVier" implementiert das Hauptfenster,
 * die Einstiegsfunktion und bindet die Spiellogik an. 
 * 
 * @author	Christian Petzold
 * 			Seminargruppe 09INB02
 * 			Matrikelnummer 50467
 * @version	1.0
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;

@SuppressWarnings("serial")
public class SiebzehnUndVier extends JFrame implements ActionListener {
	private Spiel spiel = new Spiel();
	private TischPanel tischPanel = new TischPanel(spiel);
	private JToolBar gameBar = new JToolBar();
	private JToolBar playBar = new JToolBar();
	private JButton newGameButton = new JButton("Neues Spiel");
	private JButton loadGameButton = new JButton("Spiel laden");
	private JButton saveGameButton = new JButton("Spiel speichern");
	private JButton takeCardButton = new JButton("Noch eine Karte");
	private JButton noMoreCardButton = new JButton("Keine Karte mehr");
	private JButton nextRoundButton = new JButton("Naechste Runde");
	private JLabel infoLabel = new JLabel();
	
	public SiebzehnUndVier() {
		// Fenster
		setTitle("Siebzehn und Vier in Java ~ By Christian Petzold");
		setSize(640, 480);
		setMinimumSize( getSize() );
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Leiste Oben
		gameBar.setFloatable(false);
		newGameButton.addActionListener(this);
		gameBar.add(newGameButton);		
		loadGameButton.addActionListener(this);
		gameBar.add(loadGameButton);		
		saveGameButton.addActionListener(this);
		gameBar.add(saveGameButton);		

		// Leiste Unten
		playBar.setFloatable(false);
		takeCardButton.addActionListener(this);
		playBar.add(takeCardButton);		
		noMoreCardButton.addActionListener(this);
		playBar.add(noMoreCardButton);		
		nextRoundButton.addActionListener(this);
		playBar.add(nextRoundButton);
		playBar.addSeparator(new Dimension(10,10));
		playBar.add(infoLabel);
				
		add(tischPanel, BorderLayout.CENTER);
		add(gameBar, BorderLayout.NORTH);
		add(playBar, BorderLayout.SOUTH);

		updateGUI();
	}
	
	/**
	 * Aktionen der Buttons
	 */
	public void actionPerformed(ActionEvent event)
	{
		Object source = event.getSource();
		
		if (source == newGameButton)
			spiel.neuesSpiel();
		
		if (source == loadGameButton) {
			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				try {
					InputStream fis = new FileInputStream(fileChooser.getSelectedFile());
					ObjectInputStream i = new ObjectInputStream(fis);
					spiel = (Spiel)i.readObject();
					fis.close();
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Datei wurde nicht gefunden!", "FEHLER", JOptionPane.ERROR_MESSAGE);
				} catch (ClassNotFoundException e) {
					JOptionPane.showMessageDialog(null, "Fehler beim Laden! Datei-Inhalt ungueltig", "FEHLER", JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "I/O Fehler beim Laden!", "FEHLER", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		if (source == saveGameButton) {
			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				try {
					OutputStream fos = new FileOutputStream(fileChooser.getSelectedFile());
					ObjectOutputStream o = new ObjectOutputStream(fos);
					o.writeObject(spiel);
					fos.close();
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null, "Fehler beim Speichern!", "FEHLER", JOptionPane.ERROR_MESSAGE);
				}
			}
		}
		
		if (source == takeCardButton)
			spiel.nimmKarte();
		
		if (source == noMoreCardButton)
			spiel.genugKarten();
		
		if (source == nextRoundButton)
			spiel.naechsteRunde();
		
		updateGUI();
	}

	/**
	 * Buttons und Label je nach Spielzustand setzen, Tisch neuzeichnen
	 */
	private void updateGUI() {
		takeCardButton.setEnabled( spiel.isAmSpielen() );
		noMoreCardButton.setEnabled( spiel.isAmSpielen() );
		nextRoundButton.setEnabled( !spiel.isAmSpielen() );

		tischPanel.repaint();
		if ( spiel.isAmSpielen() ) 
			infoLabel.setText("");
		else {
			if (spiel.getLastGewinner() == spiel.getSpieler()) {
				infoLabel.setForeground(Color.BLUE);
				infoLabel.setText("GEWONNEN :-)");
			} else {
				infoLabel.setForeground(Color.RED);
				infoLabel.setText("VERLOREN :-(");
			}
		}
	}
	
	/**
	 * Einstiegsfunktion
	 */
	public static void main(String[] args) {
		/*try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
		    // handle exception
		} catch (ClassNotFoundException e) {
		    // handle exception
		} catch (InstantiationException e) {
		    // handle exception
		} catch (IllegalAccessException e) {
		    // handle exception
		}*/

		SiebzehnUndVier siebzehnUndVier = new SiebzehnUndVier();
		
		siebzehnUndVier.setVisible(true);
	}
}
