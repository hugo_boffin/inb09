#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	pid_t pid1,pid2,fertig1=0,fertig2=0;
	int i,status = 0,fertig = 0;
	char filename[100] = "bsys06child", c1max[100] = "",c1int[100] = "",c2max[100] = "",c2int[100] = "";
	char *cargv[4] = {filename, NULL, NULL, NULL};

	for (i = 1; i<argc; i+=2) {
		if (strcmp(argv[i],"-c1max")==0)
			strcpy(c1max, argv[i+1]);
		else if (strcmp(argv[i],"-c1int")==0)
			strcpy(c1int, argv[i+1]);
		else if (strcmp(argv[i],"-c2max")==0)
			strcpy(c2max, argv[i+1]);
		else if (strcmp(argv[i],"-c2int")==0)
			strcpy(c2int, argv[i+1]);
	}

	if (!strlen(c1max)) {
		printf("Maximum von Kind 1: ");
		gets(c1max);
	}
	if (!strlen(c1int)) {
		printf("Ausgabeint. von Kind 1: ");
		gets(c1int);
	}
	if (!strlen(c2max)) {
		printf("Maximum von Kind 2: ");
		gets(c2max);
	}
	if (!strlen(c2int)) {
		printf("Ausgabeint. von Kind 2: ");
		gets(c2int);
	}

	pid1 = fork();
	switch (pid1) {
		case -1: /* fehler */
			perror("fork");
			exit( 1 );
		case 0: /* child */
			printf( "Child 1: Parent PID: %d, Child PID %d\n", getppid(), getpid() );
			cargv[1] = c1max;
			cargv[2] = c1int;
			execv("bsys06child",cargv);
			perror("execv");
			exit( 1 );
		default: /* parent, pid ist vom child */
			pid2 = fork();
			switch (pid2) {
				case -1: /* fehler */
					perror("fork");
					exit( 1 );
				case 0: /* child */
					printf( "Child 2: Parent PID: %d, Child PID %d\n", getppid(), getpid() );
					cargv[1] = c2max;
					cargv[2] = c2int;
					execv("bsys06child",cargv);
					perror("execv");
					exit( 1 );
				default: /* parent, pid ist vom child */
					printf( "Parent: Parent PID: %d\n", getpid() );
					while ( !fertig ) {
						//printf("%d %d %d\n",getpid(), pid1, pid2);
						if(fertig1==0) {
							if( (fertig1=waitpid(pid1,&status,WNOHANG)) != 0 )
								printf("Parent: Child1(%d) ENDE - Status |%x|%x|\n",
									pid1,(status>>8)&0xFF,status&0xFF);
						}
						if(fertig2==0) {
							if( (fertig2=waitpid(pid2,&status,WNOHANG)) != 0 )
								printf("Parent: Child2(%d) ENDE - Status |%x|%x|\n",
									pid2,(status>>8)&0xFF,status&0xFF);
						}
						fertig = (fertig1 != 0) && (fertig2 != 0);
					}

					exit( 0 );
			}
	}

	return 0;
}

