#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

short isPrime(long p);

int main(int argc, char *argv[]) {
	if (argc != 3)
		return 1;

	long i, anz=0;
	long n = atoi(argv[1]);
	long intervall = atoi(argv[2]);
	int status=0;
	pid_t pid = getpid();

	for ( i = 2; i <= n; i++ )
		if ( isPrime(i) && ++anz%intervall==0 ) 
			printf ("%d: %ld\n", pid, i);
	printf ("%d: %ld Primzahlen\n", pid, anz);

	return 0;
}

short isPrime(long p) {
	long s, d;
	s = (long) sqrt(p) + 1;
	if ( p <  2 ) return 0;
	if ( p == 2 ) return 1;
	if ( p % 2 == 0 ) return 0;
	for ( d = 3; d <= s; d+=2 )
		if ( p % d == 0 ) return 0;
	return 1;
}

