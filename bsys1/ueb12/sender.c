#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[]) {
	int index = 0;
	mode_t mode;
	int rc;
	char fname[100];
	char eab[100];
	struct stat buf;
  
	if (argc != 2) {
		printf("Es muss genau eine Zahl als Parameter angegeben werden!\n");
		return -1;
	}
	
	sscanf(argv[1],"%d",&index);
	if (index<1 || index>3) {
		printf("Ungueltiger Pipeindex\n");
		return -1;
	}	
	sprintf(fname,"PIPE%d",index);
	
	rc = stat (fname, &buf);
	if (rc < 0) {
		if (errno != ENOENT) {
			perror("stat");
			exit (1);
		}
		puts ("Pipe existiert noch nicht");
		mode = 0644;
		rc = mkfifo (fname, mode);
		if (rc < 0) {
			perror("mkfifo");
			exit (2);
		} else {
			printf ("Pipe %s wurde angelegt\n", fname);
		}
	} else {
		if ( !S_ISFIFO(buf.st_mode)) {
			printf ("File %s existiert, ist keine Pipe!\n", fname);
			exit (3);
		}
		printf ("Pipe %s existiert bereits\n", fname);
		exit(4);
	}
	
	FILE* pd = fopen(fname,"w");
	if (!pd) {
		perror("fopen");
		return -1;
	}
	
	while ( fgets(eab, sizeof(eab), stdin) ) {				
		if ( fputs(eab,pd) < 0 ) {
			perror("fwrite");
			return -1;
		}
		fflush(pd);
	}
	
	fclose(pd);
	
	return 0;
}

