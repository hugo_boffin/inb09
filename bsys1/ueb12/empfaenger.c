#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[]) {
	pid_t pid;
	int rc,ende=0,i;
	char fname[100],eab[100];
	mode_t mode;
	struct stat buf;
	FILE* pd = 0;
	int offen[3] = {0};
	int pipecount = 0;
  	
	while (!ende) {
		for (i = 0; i < 3; i++) {
			sprintf(fname,"PIPE%d",i+1);
			rc = stat (fname, &buf);
			if (rc < 0) {
				if (errno != ENOENT) {
					perror("stat");
					exit (1);
				}
				if (offen[i]) {
					printf ("Pipe %s geschlossen!\n", fname);
					offen[i] = 0;
					pipecount--;
					if (!pipecount)
						ende = 1;
				}
			} else if (!offen[i]) {
				if ( !S_ISFIFO(buf.st_mode)) {
					printf ("File %s existiert, ist aber keine Pipe!\n", fname);
					exit (3);
				}
				printf ("Pipe %s geoeffnet!\n", fname);
				offen[i] = 1;
				pipecount++;
				
				pid = fork();
				switch (pid) {
					case -1:
						perror("fork");
						exit( -1 );
					case 0:
						pd = fopen(fname,"r");
						if (!pd) {
							perror("fopen");
							return -1;
						}
						
						while ( fgets(eab, sizeof(eab), pd) ) {				
							if ( fputs(eab,stdout) < 0 ) {
								perror("fputs");
								return -1;
							}
						}
						
						fclose(pd);
						
						rc = unlink(fname);
						if (rc<0) {
							perror("unlink");
							return -1;
						}
						
						exit( 0 );
					default:
						/* Parent Prozess, weiter in Hauptschleife */
					break;
				}
			}
		}
	}
	
	return 0;
}

