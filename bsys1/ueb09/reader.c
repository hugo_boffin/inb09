#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* arg 1 filename */

int writerFertig = 0;

void signalfunc(int signr);

int main(int argc, char *argv[]) {
	if (argc != 3)
		return 1;

	char* filename = argv[1];
	int fd = atoi(argv[2]);
	int anz = 0;
	char zeile[2048];

	sigset(SIGUSR1, signalfunc); /* REGISTRIERE USR1 SIGNAL FUER START */
	kill(getppid(), SIGUSR1); /* SENDE AN PARENT */
	while (!writerFertig); /* AUF BEREITSCHAFTSSIGNAL WARTEN */

	printf ("Reader Child (%d) gibt File \"%s\" aus\n", getpid(), filename);

	/* Einlesen */
	while ( (anz = read(fd,zeile,2047) ) ) {
		zeile[anz] = '\0';
		fputs(zeile,stdout);
	}
	printf("\n\n");
	
	close(fd);

	return 0;
}

void signalfunc(int signr) {
	switch (signr) {
	case SIGUSR1:
		writerFertig=1;
		break;
	default:
		break;
	}
}
