#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* arg 1 filename */
/* arg 2 reader pid */
/* arg 3 file deskriptor */

int main(int argc, char *argv[]) {
	if (argc != 4)
		return 1;

	char *filename = argv[1];
	pid_t child2 = atoi(argv[2]);
	int fd = atoi(argv[3]);
	char zeile[2048];

	printf ("Writer Child (%d) wartet auf Eingabe nach File \"%s\"\n", getpid(), filename);

	/* Einlesen */
	while ( fgets(zeile,sizeof(zeile),stdin) ) {
		write(fd, zeile, strlen(zeile));
	}
		
	close(fd);
	printf("\n\n");
	kill(child2, SIGUSR1);

	return 0;
}
