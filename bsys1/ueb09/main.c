#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int readerReady = 0;

void signalfunc(int signr);

int main(int argc, char *argv[]) {
	char filename[100] = "";
	char fdstr[100] = "";
	char pidstr[100] = "";
	char action[10] = "";
	char kdo1[200],kdo2[200];
	int exit1, exit2;
	int fd = 0;
	pid_t pid1,pid2,fertig1=0,fertig2=0;
	int status = 0, fertig = 0;
	char *c1argv[4] = {"reader", NULL, NULL, NULL };
	char *c2argv[5] = {"writer", NULL, NULL, NULL, NULL };

	sigset(SIGUSR1, signalfunc);

	/* Filename */
	if (argc>1)
		strcpy(filename,argv[1]);
	else {
		printf("Filename: ");
		gets(filename);
	}

	c1argv[1] = filename;
	c2argv[1] = filename;

	/* Existiert File ? */
	if ( (fd = open(filename,O_WRONLY|O_CREAT|O_EXCL,0644 ))<0 ) {
		close(fd);
		printf("Datei fortsetzen (a) / Neuanlegen (sonst) ? ");
		gets(action);
		if (strcmp(action,"a") == 0)
		    fd = open(filename,O_WRONLY|O_APPEND);
		else
		    fd = open(filename,O_WRONLY|O_TRUNC);
	} else {
		fd = open(filename,O_WRONLY|O_CREAT,0644);
	}


	pid1 = fork();
	switch (pid1) {
		case -1:
			perror("fork");
			exit( 1 );
		case 0:
			close(fd);
			fd = open(filename, O_RDONLY);

			sprintf(fdstr, "%d", fd);
			c1argv[2] = fdstr;

			execv(c1argv[0],c1argv);
			perror("execv");
			exit( 1 );
		default:
			while (!readerReady); /* Auf Bereitschaftssignal warten... */

			sprintf(pidstr, "%d", pid1);
			c2argv[2] = pidstr;
			sprintf(fdstr, "%d", fd);
			c2argv[3] = fdstr;

			pid2 = fork();
			switch (pid2) {
				case -1:
					perror("fork");
					exit( 1 );
				case 0:
					execv(c2argv[0],c2argv);
					perror("execv");
					exit( 1 );
				default:
					while ( !fertig ) {
						if(fertig1==0) {
							if( (fertig1=waitpid(pid1,&status,WNOHANG)) != 0 )
								printf("Reader (%d) ENDE - Status |%x|%x|\n",
									pid1,(status>>8)&0xFF,status&0xFF);
						}
						if(fertig2==0) {
							if( (fertig2=waitpid(pid2,&status,WNOHANG)) != 0 )
								printf("Writer (%d) ENDE - Status |%x|%x|\n",
									pid2,(status>>8)&0xFF,status&0xFF);
						}
						fertig = (fertig1 != 0) && (fertig2 != 0);
					}
			
					exit( 0 );
			}
	}

	return 0;
}

void signalfunc(int signr) {
	switch (signr) {
	case SIGUSR1:
		readerReady=1;
		break;
	default:
		break;
	}
}
