#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Signalliste: kill -l

int main(int argc, char *argv[]) {
	pid_t pid1,pid2,fertig1=0, fertig2=0;
	int status = 0, fertig = 0;

	char *argv1[4] = {"prime", "99999999"};
	char *argv2[3] = {"eingabe", NULL, NULL};
	char pidarg[10];

	pid1 = fork();
	switch (pid1) {
		case -1:
			perror("fork");
			exit( 1 );
		case 0:
			execv(argv1[0],argv1);
			perror("execv");
			exit( 1 );
		default:
			pid2 = fork();
			switch (pid2) {
				case -1:
					perror("fork");
					exit( 1 );
				case 0:
					sprintf(pidarg,"%d",pid1);
					argv2[1] = pidarg;
					execv(argv2[0],argv2);
					perror("execv");
					exit( 1 );
				default:
					printf( "Parent: PID %d\n", getpid() );
					while ( !fertig ) {
						if(fertig1==0) {
							if( (fertig1=waitpid(pid1,&status,WNOHANG)) != 0 )
								printf("Parent: Child1(%d) ENDE - Status |%x|%x|\n",
									pid1,(status>>8)&0xFF,status&0xFF);
						}
						if(fertig2==0) {
							if( (fertig2=waitpid(pid2,&status,WNOHANG)) != 0 )
								printf("Parent: Child2(%d) ENDE - Status |%x|%x|\n",
									pid2,(status>>8)&0xFF,status&0xFF);
						}
						fertig = (fertig1 != 0) && (fertig2 != 0);
					}

					exit( 0 );
			}
	}

	return 0;
}

