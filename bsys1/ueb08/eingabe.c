#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

// kill(pid, signr)

int main(int argc, char *argv[]) {
	char eingabe[128];

	printf( "Eingabe Child: Parent PID: %d, Child PID %d\n", getppid(), getpid() );

	if (argc != 2)
		return 1;

	pid_t pid = atoi(argv[1]);
	int rc;
	int signr;

	while ( fgets(eingabe,sizeof(eingabe),stdin) ) {
		signr = atoi(eingabe);

		switch (signr) {
		case SIGINT:
			rc = kill(pid, SIGINT);
			if (rc<0)
				perror("kill");
			break;
		case SIGQUIT:
			rc = kill(pid, SIGQUIT);
			if (rc<0)
				perror("kill");
			return;
		case SIGUSR1:
			rc = kill(pid, SIGUSR1);
			if (rc<0)
				perror("kill");
			break;
		case SIGUSR2:
			rc = kill(pid, SIGUSR2);
			if (rc<0)
				perror("kill");
			break;
		default:
			printf("Ungueltige Eingabe!\n");
		}
	}

	return 0;
}
