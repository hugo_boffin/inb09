#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <signal.h>

int sigint_c=0,sigquit_c=0,sigusr1_c=0,sigusr2_c=0;
int sigint_set=0,sigquit_set=0,sigusr1_set=0,sigusr2_set=0;

short isPrime(long p);
void signalfunc(int signr);

int main(int argc, char *argv[]) {
	printf( "Primzahl Child: Parent PID: %d, Child PID %d\n", getppid(), getpid() );

	if (argc != 2)
		return 1;

	sigset(SIGINT,  signalfunc);
	sigset(SIGQUIT, signalfunc);
	sigset(SIGUSR1, signalfunc);
	sigset(SIGUSR2, signalfunc);

	long i, anz = 0;
	long n = atoi(argv[1]);

	for ( i = 2; i <= n; i++ ) {
		/* Primzahltest */
		if ( isPrime(i) ) 
			anz++;

		/* Signalbehandlung */
		if (sigint_set) {
			printf("SIGINT = %d\n",sigint_c);
			printf("SIGQUIT = %d\n",sigquit_c);
			printf("SIGUSR1 = %d\n",sigusr1_c);
			printf("SIGUSR2 = %d\n",sigusr2_c);
			sigint_set = 0;
		}
		if (sigquit_set) {
			break;
		}
		if (sigusr1_set) {
			printf("Bei %ld, %ld Primzahlen gefunden!\n",i,anz);
			sigusr1_set = 0;
		}
		if (sigusr2_set) {
			printf("Neustart!\n",anz);
			i=2;
			anz=0;
			sigusr2_set = 0;
		}
	}

	return 0;
}

void signalfunc(int signr) {
	switch (signr) {
	case SIGINT:
		sigint_c++;
		sigint_set=1;
		break;
	case SIGQUIT:
		sigquit_c++;
		sigquit_set=1;
		break;
	case SIGUSR1:
		sigusr1_c++;
		sigusr1_set=1;
		break;
	case SIGUSR2:
		sigusr2_c++;
		sigusr2_set=1;
		break;
	default:
		break;
	}
}

short isPrime(long p) {
	long s, d;
	s = (long) sqrt(p) + 1;
	if ( p <  2 ) return 0;
	if ( p == 2 ) return 1;
	if ( p % 2 == 0 ) return 0;
	for ( d = 3; d <= s; d+=2 )
		if ( p % d == 0 ) return 0;
	return 1;
}

