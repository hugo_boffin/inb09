#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int parent_main();
int child_main();
short isPrime(long p);

int main() {
	pid_t pid;
	pid = fork();
	switch (pid) {
		case -1:
			perror("fork");
			exit( 1 );
		case 0: /* child */
			exit( child_main() );
		default: /* parent */
			exit( parent_main() );
	}

	return 0;
}

int parent_main() {
	long i, n=4000000, anz=0;
	pid_t pid = getpid();
	printf( "Parent -> Parent PID: %d\n", pid );
	int status=0;

	/* Primzahlen */
	for ( i = 2; i <= n; i++ )
		if ( isPrime(i) && ++anz%2000==0 ) 
			printf ("%d: %ld\n", pid, i);
	printf ("%d: %ld Primzahlen\n", pid, anz);

	/* Ende, warten auf Kind(er) */
	while ( (pid = wait(&status)) > 0 ) {
		printf("Parent -> Child-PID=%d, Status |%x|%x|\n",
			pid, (status>>8)&0xFF,status&0xFF);
	}

	return 0;
}

int child_main() {
	long i, n=5000000, anz=0;
	pid_t pid = getpid();
	pid_t ppid = getppid();
	printf( "Child -> Parent PID: %d, Child PID %d\n", ppid, pid );

	/* Primzahlen */
	for ( i = 2; i <= n; i++ )
		if ( isPrime(i) && ++anz%2000==0 ) 
			printf ("%d: %ld\n", pid, i);
	printf ("%d: %ld Primzahlen\n", pid, anz);

	return 0;
}

short isPrime(long p) {
	long s, d;
	s = (long) sqrt(p) + 1;
	if ( p <  2 ) return 0;
	if ( p == 2 ) return 1;
	if ( p % 2 == 0 ) return 0;
	for ( d = 3; d <= s; d+=2 )
		if ( p % d == 0 ) return 0;
	return 1;
}

