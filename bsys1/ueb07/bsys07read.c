#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// arg 1 filename
// arg 2 filedeskriptor

int main(int argc, char *argv[]) {
	if (argc != 3)
		return 1;

	char *filename = argv[1];
	FILE *filehandle = fdopen(atoi(argv[2]),"ab");
	char zeile[2048];

	freopen(filename,"rb",filehandle);
	rewind(filehandle);

	printf ("Reader Child (%d) gibt File \"%s\" aus\n", getpid(), filename);

	/* Einlesen */
	while ( fgets(zeile,sizeof(zeile),filehandle) ) {
		fputs(zeile,stdout);
	}
	
	printf("\n\n");

	return 0;
}
