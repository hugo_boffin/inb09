#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char filename[100] = "";
	char action[10] = "";
	char kdo1[200],kdo2[200];
	FILE* fileHandle = NULL;
	int exit1, exit2;

	/* Filename */
	if (argc>1)
		strcpy(filename,argv[1]);
	else {
		printf("Filename: ");
		gets(filename);
	}

	/* Existiert File ? */
	if ( fileHandle = fopen(filename, "rb") ) {/* JA -> Abfrage Anlegen/Forsetzen */
		printf("Datei fortsetzen (a) / Neuanlegen (sonst) ? ");
		gets(action);
		if (strcmp(action,"a") == 0)
			freopen(filename, "ab", fileHandle);
		else
			freopen(filename, "wb", fileHandle);
	} else { /* NEIN -> Neu anlegen */
		fileHandle = fopen(filename, "wb");
	}

	sprintf(kdo1,"./bsys07write \"%s\" %d", filename, fileno(fileHandle) );
	sprintf(kdo2,"./bsys07read \"%s\" %d", filename, fileno(fileHandle) );

	exit1 = system(kdo1);
	exit2 = system(kdo2);

	printf("Writer Child Exit Status |%i|\n",exit1);
	printf("Reader Child Exit Status |%i|\n",exit2);

	fclose(fileHandle);

	return 0;
}

