#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main(int argc, char *argv[]) {
	int rc,ende=0,i;
	char fname[100],eab[100];
	mode_t mode;
	struct stat buf;
	int pd[3] = {0};
	int offen[3] = {0};
	int pipecount = 0;
  	
	while (!ende) {
		for (i = 0; i < 3; i++) {
			sprintf(fname,"PIPE%d",i+1);
			
			if (offen[i]) {
				rc = read(pd[i], eab, sizeof(eab));
				if (rc == 0) {
					close(pd[i]);
					rc = unlink(fname);
					if (rc<0) {
						perror("unlink");
						return -1;
					}
					printf ("Pipe %s geschlossen!\n", fname);
					offen[i] = 0;
					pipecount--;
					if (!pipecount)
						ende = 1;
				} else if (rc>0) {
					//printf("%d %d\n",strlen(eab),rc);
					eab[rc] = '\0';
					fputs(eab,stdout);
				}
			} else {
				rc = stat (fname, &buf);
				if (rc < 0) {
					if (errno != ENOENT) {
						perror("stat");
						exit (1);
					}
				} else {
					if ( !S_ISFIFO(buf.st_mode)) {
						printf ("File %s existiert, ist aber keine Pipe!\n", fname);
						exit (3);
					}
					printf ("Pipe %s geoeffnet!\n", fname);
					offen[i] = 1;
					pipecount++;
					
					pd[i] = open(fname,O_RDONLY|O_NONBLOCK);
					if (!pd[i]) {
						perror("open");
						return -1;
					}
				}
			}
		}
	}
	
	return 0;
}

