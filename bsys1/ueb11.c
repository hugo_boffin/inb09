#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int child(int *fd, int nr);

int main(int argc, char *argv[]) {
	int fd1[2],fd2[2],rc,l,status,ende1=0,ende2=0;
	pid_t pid;
	char eab[256] = "";
	
	if ( (rc=pipe(fd1)) < 0 ) {
		perror("pipe");
		return -1;
	}
	
	pid = fork();
	switch (pid) {
		case -1:
			perror("fork");
			exit( -1 );
		case 0:
			exit( child(fd1, 1) );
		default:
			close(fd1[1]);
			
			if ( (rc=pipe(fd2)) < 0 ) {
				perror("pipe");
				return -1;
			}
			
			pid = fork();
			switch (pid) {
				case -1:
					perror("fork");
					exit( -1 );
				case 0:
					// Vom Ersten Parent Pipe eine noch offen!!
					exit( child(fd2, 2) );
				default: {
					//printf( "Parent: Parent PID: %d\n", getpid() );
					close(fd2[1]);
					
					while (!ende1 || !ende2) {
						if (!ende1) { 
							// Start
							if ( (l=write(fd1[0],"Los",4)) < 0 ) {
								perror("write parent 1");
								return -1;
							}
							//fflush();
							
							// Empfang
							if ( (l = read(fd1[0],eab,sizeof(eab))) < 0) {
								perror("read parent 1");
								return -1;
							} else if ( l == 0) {
								ende1 = 1;
								if ( (rc=close(fd1[0])) < 0 ) {
									perror("close parent 1");
									return -1;
								}
							} else {
								printf("Child 1 Output: %s", eab);
							}
						}
						
						if (!ende2) { 
							// Start
							if ( (l=write(fd2[0],"Los",4)) < 0 ) {
								perror("write parent 2");
								return -1;
							}
							//fflush();
							
							// Empfang
							if ( (l = read(fd2[0],eab,sizeof(eab))) < 0) {
								perror("read parent 2");
								return -1;
							} else if ( l == 0) {
								ende2 = 1;
								if ( (rc=close(fd2[0])) < 0 ) {
									perror("close parent 2");
									return -1;
								}
							} else {
								printf("Child 2 Output: %s", eab);
							}
						}
					}
					
					while ( (pid = wait(&status)) > 0 ) {
						printf("Parent -> Child-PID=%d, Status |%x|%x|\n",
							pid, (status>>8)&0xFF,status&0xFF);
					}
					exit( 0 );
				}
			}
	}
	return 0;
}

int child(int *fd, int nr) {
	int rc,l,fertig=0;
	char eab[256] = "";
	
	//printf( "Child %i: Parent PID: %d, Child PID %d\n", nr, getppid(), getpid() );
			
	if ( (rc=close(fd[0])) < 0 ) {
		perror("close");
		return -1;
	}
	
	do {
		// Empfang
		if ( (l = read(fd[1],eab,sizeof(eab))) < 0) {
			perror("read child");
			return -1;
		} else if ( l == 0) {
			printf("Fertig %i\n:",nr);
			fertig = 1;
		} else {
			printf("Child %i Input:",nr);
			
			if ( fgets(eab, sizeof(eab), stdin) ) {				
				// Senden
				if ( (l=write(fd[1],eab,sizeof(eab))) < 0 ) {
					perror("write child");
					return -1;
				}
			} else {
				printf(" -> Ende %i\n",nr);
				fertig = 1;
			}
		}
	} while (!fertig);
	
	if ( (rc=close(fd[1])) < 0 ) {
		perror("close");
		return -1;
	}
	
	return 0;
}
