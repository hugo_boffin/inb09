#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <signal.h>

short resetFlag = 0;

int calcPrimes();
void signalFunc(int signr);
short isPrime(long p);

int main() {
	struct itimerval itime;
	int anz;

	sigset(SIGALRM, signalFunc);
	sigset(SIGVTALRM, signalFunc);

	timerclear(&itime.it_value);
	timerclear(&itime.it_interval);

	itime.it_value.tv_sec = 1;
	itime.it_interval.tv_sec = 1;
	setitimer(ITIMER_REAL, &itime, NULL); /* Timer starten */

	anz = calcPrimes();
	printf("ITIMER_REAL -> %d\n", anz);

	timerclear(&itime.it_value);
	timerclear(&itime.it_interval);
	setitimer(ITIMER_REAL, &itime, NULL); /* Timer anhalten */

	itime.it_value.tv_sec = 1;
	itime.it_interval.tv_sec = 1;
	setitimer(ITIMER_VIRTUAL, &itime, NULL); /* Timer starten */

	anz = calcPrimes();
	printf("ITIMER_VIRTUAL -> %d\n", anz);

	timerclear(&itime.it_value);
	timerclear(&itime.it_interval);
	setitimer(ITIMER_VIRTUAL, &itime, NULL); /* Timer anhalten */

	return 0;
}

int calcPrimes() {
	//int anz = 0;
	int gesamt = 0;
	int repeat = 0;
	int i = 2;
	while (repeat<10) {
		//if ( isPrime(i++) )
		isPrime(i++);
		//anz++;
		if (resetFlag) {
			repeat++;
			printf("%d: %d\n", repeat, i);
			resetFlag=0;
			gesamt += i;
			i=2;
			//anz = 0;
		}
	}
	return gesamt/10;
}

void signalFunc(int signr) {
	switch (signr) {
	case SIGALRM:
		resetFlag=1;
		break;
	case SIGVTALRM:
		resetFlag=1;
		break;
	default:
		break;
	}
}

short isPrime(long p) {
	long s, d;
	s = (long) sqrt(p) + 1;
	if ( p <  2 ) return 0;
	if ( p == 2 ) return 1;
	if ( p % 2 == 0 ) return 0;
	for ( d = 3; d <= s; d+=2 )
		if ( p % d == 0 ) return 0;
	return 1;
}
