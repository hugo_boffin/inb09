--import Test.SmallCheck

assoc f x y z = f x (f y z) == f (f x y) z

--assoc (+) 1 2 3
--assoc (-) 1 2 3
--test ( assoc (+) )
--test ( assoc (-) )
--test ( \ xs ys -> length (xs ++ ys) == length xs + ys )