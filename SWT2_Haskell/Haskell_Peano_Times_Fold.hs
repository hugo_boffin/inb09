module Blueprint where

data N = Z | S N deriving ( Eq, Show )

fold :: b -> (b -> b) -> (N -> b)
fold z s x = case x of
    Z -> z
    S x' -> s (fold z s x')

plus :: N -> N -> N
--plus x y = ( fold y S ) x
plus x y = ( fold y (\a -> S(a)) ) x

times :: N -> N -> N
--times x y = ( fold Z (plus y) ) x
times x y = ( fold Z ((\ a b -> plus a b) y) ) x

test = and [ plus (S (S Z)) (S (S Z)) == S (S (S (S Z)))
           , plus (S (S Z)) Z == S (S Z)
           , times (S (S Z)) (S (S Z)) == S (S (S (S Z)))
           , times (S (S Z)) Z == Z
           ]
