module Blueprint where

data Colour = Blue | Green | Red 
  deriving ( Eq, Show )

data Car = Car { wheels :: Integer
               , colour :: Colour
               }
  deriving ( Eq, Show )

data Property = Colour_Is Colour
              | Wheels Ordering Integer
              | And Property Property
              | Not Property 
  deriving Show 

check :: Property -> Car -> Bool
check prop car = case prop of
    Colour_Is col -> col == colour car
    Wheels ord num -> case ord of
		EQ -> num == wheels car
		LT -> num > wheels car
		GT -> num < wheels car
    And l r -> (check l car) && (check r car)
    Not p -> not (check p car)

cars = [ Car { wheels = 4, colour = Red }
        , Car { wheels = 2, colour = Blue }
        , Car { wheels = 14, colour = Green }
        , Car { wheels = 4, colour = Green }
        , Car { wheels = 2, colour = Red }
        ]

prop1 :: Property
prop1 = And (Not (And (Wheels EQ 4) (Colour_Is Green))) (Not (And (Wheels EQ 2) (Colour_Is Red)))

test :: Bool
test = and
    [ check ( Wheels EQ 4 ) ( cars !! 0 )
    , check ( Wheels LT 3 ) ( cars !! 1 )
    , check ( And ( Wheels EQ 14 ) ( Colour_Is Green )) ( cars !! 2 )
    , check ( Not ( Colour_Is Red ) ) ( cars !! 3 )
    , filter ( check prop1 ) cars == take 3 cars
    ]
