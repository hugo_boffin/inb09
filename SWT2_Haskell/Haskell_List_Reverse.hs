module Blueprint where

import Prelude hiding ( reverse )

data List a = DieNull | Nachfolger a ( List a ) deriving ( Eq, Show )

append :: List a -> List a -> List a
append x y = case x of
        DieNull -> y
        Nachfolger a x' -> Nachfolger (a) (append x' y)

reverse :: List  b -> List b 
reverse l = case l of
         DieNull -> DieNull
         Nachfolger b l'  -> append(reverse l')(Nachfolger b DieNull)

test :: Bool
test = reverse (Nachfolger 1 (Nachfolger 2 (Nachfolger 3 DieNull))) == Nachfolger 3 (Nachfolger 2( Nachfolger 1 DieNull))