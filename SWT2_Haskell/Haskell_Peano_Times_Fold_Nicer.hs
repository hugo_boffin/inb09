module Blueprint where

data Zahl = Nix | EinsPlus Zahl deriving ( Eq, Show )

fold :: b -> (b -> b) -> (Zahl -> b)
fold z s x = case x of
    Nix -> z
    EinsPlus x' -> s (fold z s x')

--plus2 :: Zahl -> Zahl -> Zahl
--plus2 z x = case x of
--	Nix -> z
--	EinsPlus x' -> EinsPlus(plus2 z x')
	
plus :: Zahl -> Zahl -> Zahl
plus x y = ( fold y EinsPlus ) x


times2 :: Zahl -> Zahl -> Zahl
times2 z x = case x of
        Nix -> Nix
        EinsPlus x' -> plus z (times2 z x')
		
times :: Zahl -> Zahl -> Zahl
times x y = ( fold Nix (plus y) ) x

test = and [ plus (EinsPlus (EinsPlus Nix)) (EinsPlus (EinsPlus Nix)) == EinsPlus (EinsPlus (EinsPlus (EinsPlus Nix)))
           , plus (EinsPlus (EinsPlus Nix)) Nix == EinsPlus (EinsPlus Nix)
           , times (EinsPlus (EinsPlus Nix)) (EinsPlus (EinsPlus Nix)) == EinsPlus (EinsPlus (EinsPlus (EinsPlus Nix)))
           , times (EinsPlus (EinsPlus Nix)) Nix == Nix
           ]
