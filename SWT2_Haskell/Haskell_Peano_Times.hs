module Blueprint where

data N = Z | S N deriving ( Eq, Show )

plus :: N -> N -> N
plus x y = case x of
       Z -> y
       S x' -> S(plus x' y)

times :: N -> N -> N
times x y = case y of
        Z -> Z
        S y' -> plus x (times x y')
 
test :: Bool
test = and 
  [ times (S (S Z)) (S (S (S Z))) == S(S(S(S(S (S Z)))))
  , times (S (S Z)) Z == Z
  ]