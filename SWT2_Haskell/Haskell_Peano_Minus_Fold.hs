module Blueprint where

data N = Z | S N deriving ( Eq, Show )

fold :: b -> (b -> b) -> (N -> b)
fold z s x = case x of
    Z -> z
    S x' -> s (fold z s x')

data Pair a b = Pair a b deriving ( Eq, Show )

pre' :: N -> Pair N N
pre' = fold undefined
            ( \ x -> case x of
                  Pair y z -> undefined
            )

pre :: N -> N
pre x = case pre' x of
    Pair y z -> z

minus :: N -> N -> N
minus x y = ( fold undefined undefined ) y


test = and [ pre' (S (S (S Z))) == Pair (S (S (S Z))) (S (S Z))
           , pre' (S Z) == Pair (S Z) Z
           , pre' Z == Pair Z Z
           , pre (S (S (S Z))) == S (S Z)
           , pre Z == Z
           , minus (S (S Z)) (S Z) == S Z
           , minus (S Z) (S (S Z)) == Z
           , minus (S (S Z)) (S (S Z)) == Z
           ]
