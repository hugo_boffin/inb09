module Blueprint where

import Prelude hiding ( reverse )

data List a = Nil | Cons a ( List a ) deriving ( Show, Eq )

fold :: b -> (a -> b -> b) -> ( List a -> b )
fold nil cons = \ l -> case l of
    Nil -> nil
    Cons x xs -> cons x ( fold nil cons xs )
	
--reverse2 :: List  a -> List a 
--reverse2 l = case l of
--	Nil -> Nil
--	Cons a b  -> append (reverse2 b) (Cons a Nil)
		 
append :: List a -> List a -> List a
append x y = ( fold y Cons ) x
--append x y = ( fold y (\ a b -> Cons a b) ) x 

reverse :: List a -> List a
reverse = fold Nil (\ a b -> append b (Cons a Nil )) 

test = and [ append (Cons 1 (Cons 2 Nil)) Nil == Cons 1 (Cons 2 Nil)
           , append (Cons 1 Nil) (Cons 2 Nil) == Cons 1 (Cons 2 Nil)
           , reverse (Cons 1 (Cons 2 Nil)) == Cons 2 (Cons 1 Nil)
           , reverse (Cons 1 Nil) == Cons 1 Nil
           ]
