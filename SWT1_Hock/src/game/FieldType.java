package game;

public enum FieldType {	
	AVATAR,
	FINISH,
	ITEM,
	STD,
	VOID,
	WALL;
}
