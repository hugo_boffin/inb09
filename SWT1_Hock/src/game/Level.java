package game;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class Level {
	private boolean isSolved;
	private int score;
	private int steps;
	private int stdFields;
	private Point avatarPos;
	private Point dimension;
	private List<Point> itemSpawnList;
	private Field fields[][];
	
	public Level(Point dimension) {
		isSolved = false;
		score = 0;
		steps = 0;
		stdFields = 0;
		itemSpawnList = new ArrayList<Point>();
		setDimension(dimension);
	}
	
	public Point getDimension() {
		return dimension;
	}
	
	public boolean isSolved() {
		return isSolved;
	}

	public int getSteps() {
		return steps;
	}
	
	public int getScore() {
		return score;
	}	
	
	private boolean isInBounds(Point position) {
		return position.x >= 0 && position.y >= 0 && 
			position.x < dimension.x && position.y < dimension.y;
	}
	
	public FieldType getFieldType(Point position) {
		if (isInBounds(position))
			return fields[position.x][position.y].getFieldType();
		return null;
	}
	
	public void setDimension(Point dimension) {
		this.dimension = new Point(dimension);
		fields = new Field[dimension.x][dimension.y];
		for (int i = 0; i < dimension.x; i++)
			for (int j = 0; j < dimension.y; j++)
				fields[i][j] = new Field();
	}
	
	public void setField(Point position, FieldType type) {
		Field f = fields[position.x][position.y];
		
		if (isInBounds(position)) {
			if (type == FieldType.AVATAR) 
				avatarPos = new Point(position);
			if (f.getFieldType() == FieldType.STD)
				stdFields++;
			if (type == FieldType.ITEM) {
				itemSpawnList.add(position);
				type = FieldType.STD;
			}
			
			f.setType(type);
		}
	}
	
	public void setUpDone() {
		if (itemSpawnList.size() > 0) {
			Point p = itemSpawnList.get(0);
			fields[p.x][p.y].setType(FieldType.ITEM);
			itemSpawnList.remove(0);
		}	
	}

	public Level setAvatar(Point delta) {
		Point newPos = new Point(avatarPos.x + delta.x, avatarPos.y + delta.y);
		Point jmpPos = new Point(avatarPos.x + 2 * delta.x, avatarPos.y + 2 * delta.y);
				
		if (isInBounds(newPos) && fields[newPos.x][newPos.y].isAccessible()) {
			// No Op
		} else if (isInBounds(jmpPos) && fields[jmpPos.x][jmpPos.y].isAccessible()) {
			newPos = jmpPos;
		} else {
			newPos = null;
		}
		
		if (newPos != null) {
			FieldType type = fields[newPos.x][newPos.y].getFieldType();
			
			if (type == FieldType.STD)
				stdFields--;
			
			if (type == FieldType.ITEM) {
				if (itemSpawnList.size() > 0) {
					Point p = itemSpawnList.get(0);
					fields[p.x][p.y].setType(FieldType.ITEM);
					itemSpawnList.remove(0);
				}
			}

			setField(avatarPos, FieldType.VOID);
			setField(newPos, FieldType.AVATAR);	
			steps++;		
			
			if (stdFields == 0 && type == FieldType.FINISH) {
				isSolved = true;
			}
		}
		
		return this;
	}
}
