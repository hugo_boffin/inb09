package game;

public class Field {
	private FieldType type;
	
	public Field() {
		type = FieldType.VOID;
	}	
	
	public Field(FieldType type) {
		setType(type);
	}
	
	public void setType(FieldType type) {
		this.type = type;
	}
	
	public FieldType getFieldType() {
		return type;
	}
	
	public boolean isAccessible() {
		switch (type) {
		case FINISH:
		case ITEM:
		case STD:
			return true;
		default: return false;
		}
	}
}
