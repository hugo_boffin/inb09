package std;

import java.awt.Point;

import game.FieldType;
import game.Level;


public class DefaultLevel {
	public DefaultLevel() {
		
	}
	
	public static Level getLevel() {
		Level level = new Level(new Point(15, 10));
		
		level.setField(new Point(0, 0), FieldType.AVATAR);
		level.setField(new Point(1, 0), FieldType.STD);
		level.setField(new Point(2, 0), FieldType.STD);
		level.setField(new Point(3, 0), FieldType.STD);
		level.setField(new Point(4, 0), FieldType.ITEM);
		level.setField(new Point(5, 0), FieldType.STD);
		level.setField(new Point(6, 0), FieldType.STD);
		level.setField(new Point(7, 0), FieldType.STD);
		
		level.setField(new Point(7, 2), FieldType.STD);
		level.setField(new Point(7, 3), FieldType.STD);
		level.setField(new Point(7, 4), FieldType.STD);
		
		level.setField(new Point(6, 4), FieldType.FINISH);
		
		level.setField(new Point(0, 4), FieldType.WALL);
		level.setField(new Point(1, 4), FieldType.WALL);
		level.setField(new Point(2, 4), FieldType.WALL);
		level.setField(new Point(3, 4), FieldType.WALL);
		
		level.setField(new Point(14, 9), FieldType.ITEM);
		level.setUpDone();
		
		
		return level;
	}
}
