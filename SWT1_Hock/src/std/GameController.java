package std;

import java.awt.Point;

import game.Level;
import game.Player;


public class GameController {
	@SuppressWarnings("unused")
	private static Player player;
	private static Level level;
	
	public GameController() {
		
	}
	
	public static Level getLevel() {
		level = DefaultLevel.getLevel();
		return level;
	}
	
	public static Level step(Point delta) {
		return level.setAvatar(delta);
	}
}
