package gui;

import javax.swing.JFrame;

public class Highscore extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public Highscore() {
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setBounds(200, 200, 600, 400);
		this.setLayout(null);
		this.setTitle("HoCk :: Highscore");
	}
}
