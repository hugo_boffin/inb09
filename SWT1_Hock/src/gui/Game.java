package gui;

import game.Level;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import std.GameController;

public class Game extends JFrame implements KeyListener {
	private static final int size = 50;
	private static final long serialVersionUID = 1L;
	private Level level;
	private JButton fields[][] = null;
	
	public Game() {
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setBounds(200, 200, 600, 400);
		this.setLayout(null);
		this.setTitle("HoCk :: Game");
		this.addKeyListener(this);
	}
	
	public void setLevel(Level level) {
		this.level = level;
		Point p = level.getDimension();
		this.setSize(p.x * size, p.y * size + 30);
	}

	public void paintField() {
		Point p = level.getDimension();
		fields = new JButton[p.x][p.y];
		
		for (int i = 0; i < p.x; i++)
			for (int j = 0; j < p.y; j++) {
				fields[i][j] = new JButton("");
				switch (level.getFieldType(new Point(i, j))) {	
				case AVATAR: fields[i][j].setBackground(Color.BLUE); break;
				case FINISH: fields[i][j].setBackground(Color.GREEN); break;
				case ITEM: fields[i][j].setBackground(Color.RED); break;
				case STD: fields[i][j].setBackground(Color.WHITE); break;
				case VOID: fields[i][j].setBackground(Color.GRAY); break;
				case WALL: fields[i][j].setBackground(Color.BLACK); break;
				}		
				fields[i][j].setFocusable(false);
				fields[i][j].setBounds(i * size, j * size, size, size);
				this.add(fields[i][j]);
			}	

		
		setVisible(true);
		setFocusable(true);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// No Op
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// No Op
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		char key = arg0.getKeyChar();
		Point p = level.getDimension();
		Point delta = null;

		if ("wasd".indexOf(key) != -1)
			for (int i = 0; i < p.x; i++)
				for (int j = 0; j < p.y; j++) {
					fields[i][j].setVisible(false);
					this.remove(fields[i][j]);
				}
		
		switch (key) {
		case 'w': // Oben
			delta = new Point(0, -1);
			break;
		case 'a': // Links
			delta = new Point(-1, 0);
			break;
		case 's': // Unten
			delta = new Point(0, 1);
			break;
		case 'd': // Rechts
			delta = new Point(1, 0);
			break;
		}
		
		if (delta != null) {
			level = GameController.step(delta);
			if (level.isSolved()) {
				setTitle("\\(^.^)// Solved");
			}
			paintField();
		}
	} 	
}
