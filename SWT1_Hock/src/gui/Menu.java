package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import std.GameController;

public class Menu extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JButton btnNewGame;
	private JButton btnEditor;
	private JButton btnHighscore;
	private JButton btnExit;
	
	private Game game;
	private Highscore highscore;
	
	public Menu() {
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setBounds(200, 200, 200, 230);
		this.setLayout(null);
		this.setTitle("HoCk");
		
		btnNewGame = new JButton("New Game");
		btnNewGame.setBounds(0, 0, 200, 50);
		btnNewGame.addActionListener(this);
		this.add(btnNewGame);
		
		btnEditor = new JButton("Editor");
		btnEditor.setBounds(0, 50, 200, 50);
		btnEditor.addActionListener(this);
		this.add(btnEditor);	
		
		btnHighscore = new JButton("Highscore");
		btnHighscore.setBounds(0, 100, 200, 50);
		btnHighscore.addActionListener(this);
		this.add(btnHighscore);
		
		btnExit = new JButton("Exit");
		btnExit.setBounds(0, 150, 200, 50);
		btnExit.addActionListener(this);
		this.add(btnExit);
		
		game = new Game();
		highscore = new Highscore();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnNewGame) {
			game.setLevel(GameController.getLevel());
			game.paintField();
		}
		else if (e.getSource() == btnEditor) {
			
		}
		else if (e.getSource() == btnHighscore) {
			highscore.setVisible(true);
		}
		else if (e.getSource() == btnExit) {
			System.exit(0);
		}
	}
}
