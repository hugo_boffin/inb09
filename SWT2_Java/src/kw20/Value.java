package kw20;

import javax.swing.JComponent;

interface Value {

	void update(JComponent view, Board board, Cell cell);

	void removeOption(int fill);

}