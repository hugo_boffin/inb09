package kw20;

import java.util.HashMap;
import java.util.Map;

public class Board {
	public final Map<Position, Cell> contents;
	private final int base;

	public Board(final int base) {
		this.base = base;
		contents = new HashMap<Position, Cell>();
		for (Position p : Position.all(base)) {
			contents.put(p, new Cell(this,new Empty(1, base * base)));
		}
		for (Position p : Position.all(base)) {
			for (Position q : Position.all(base)) {
				if (p.sees(q)) {
					contents.get(p).addObserver(contents.get(q));
				}
			}
		}
	}
	
//	public boolean isAllowed(Cell cell, Value newVal) {
//		if (newVal instanceof Full) {
//			for (Position p : Position.all(base)) {
//				if (contents.get(p) == cell) {
//					for (Position q : Position.all(base)) {
//						if (p.sees(q)) {
//							final Value thatVal = contents.get(q).getValue();
//							if (thatVal instanceof Full && ((Full)newVal).getFill() == ((Full)thatVal).getFill())
//								return false;
//						}
//					}
//				}
//			}	
//			return true;
//		}
//		return true;
//	}

	public void fill(Integer[][] data) {
		for (int row = 0; row < base*base; row++) {
			for (int col = 0; col < base*base; col++) {
				if (data[row][col] != null) {
					contents.get( new Position(base, row, col)).setValue(new Full(data[row][col]));
				}
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int size = (int) Math.sqrt((float)contents.size());
		int base = (int) Math.sqrt((float)size);
		for (int row = 0; row < size; row++) {
			for (int col = 0; col < size; col++) {
				Value v = ((Cell)contents.get(new Position(base,row,col))).getValue();
				if ( v instanceof Full) {
					sb.append( ((Full)v).getFill() );
					sb.append(" ");
				} else {
					sb.append("- ");
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
