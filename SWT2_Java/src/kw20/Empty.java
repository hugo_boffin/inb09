package kw20;

import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComponent;

public class Empty implements Value {
	final Set<Integer> options;
	final int base;
	
	public Empty(int lo, int hi) {
		Set<Integer> s = new HashSet<Integer>();
		for (int i = lo; i <= hi; i++) {
			s.add(i);
		}
		this.options = s;
		
		this.base = (int)Math.sqrt(hi);
	}

	public void removeOption(int fill) {
		options.remove(fill);
	}
	
	public void update(JComponent view, Board board, final Cell cell) {
		view.removeAll();
		view.setLayout(new GridLayout(base, base));
		
		for (int i = 1; i <= base*base; i++) {
			final int value = i;

			JButton b = new JButton();
			b.setEnabled( options.contains(i) );
			b.setText(Integer.toString(i));
			b.setMargin(new Insets(0,0,0,0));
			b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cell.setValue( new Full(value) );
				}
			});
			
			view.add(b);
		} 
		
		view.validate();
	}
}
