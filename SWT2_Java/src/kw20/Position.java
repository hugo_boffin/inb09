package kw20;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Position {
	private final int[] coords;

	public Position(int base, int row, int col) {
		this.coords = new int[] { row / base, row % base, col / base,
				col % base };
	}

	boolean sees(Position that) {
		return !this.equals(that)
				&& (this.coords[0] == that.coords[0] && this.coords[2] == that.coords[2] // Selber Block
					|| this.coords[0] == that.coords[0] && this.coords[1] == that.coords[1] // Selbe Spalte
					|| this.coords[2] == that.coords[2]	&& this.coords[3] == that.coords[3] // Selbe Reihe
				);
	}

	public static Iterable<Position> all(int base) {
		final Collection<Position> ps = new ArrayList<Position>();
		for (int row = 0; row < base * base; row++)
			for (int col = 0; col < base * base; col++)
				ps.add(new Position(base, row, col));

		return ps;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(coords);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (!Arrays.equals(coords, other.coords))
			return false;
		return true;
	}

}
