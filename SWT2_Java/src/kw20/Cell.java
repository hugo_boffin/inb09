package kw20;

import java.awt.Color;
import java.awt.Component;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Cell extends Observable implements Observer {

	private Value value;
	private final JPanel view;
	private final Board board;

	public Cell(Board board,Value value) {
		this.board = board;

		view = new JPanel();
		view.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		setValue(value);
	}

	public void update(Observable o, Object arg) {
		//Cell cell = (Cell)o;
		Integer i = (Integer)arg;
		
		if (i != null) { 
			value.removeOption( i );
			
			value.update(view,board,this);
		}
	}

	public Component getView() {
		return view;
	}

	public void setValue(Value value) {
		//if (board.isAllowed(this, value)) {
			this.value = value;
			value.update(view,board,this);
	
			this.setChanged(); 
			
			if (value instanceof Full)
				this.notifyObservers(Integer.valueOf(((Full)value).getFill()) );
			else
				this.notifyObservers();
        //}
	}

	public Value getValue() {
		return value;
	}
}
