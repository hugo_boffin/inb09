package kw20;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Sudoku extends JApplet {
	private final Board board;
	private final int base;
	
	public Sudoku() {
		this.base = 3;
		this.board = new Board(this.base);
		this.board.fill(start);
		
		System.out.println(this.board);
	}

	public void init() {		
		this.setSize(1024,600);
		
		this.getContentPane().setLayout(new GridLayout(base, base));
		for (int brow = 0; brow < base; brow++) {
			for (int bcol = 0; bcol < base; bcol++) {
				
				final JPanel blockPanel = new JPanel();
				blockPanel.setLayout(new GridLayout(base, base));
				blockPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

				for (int row = brow*base; row < (brow+1)*base; row++) {
					for (int col = bcol*base; col < (bcol+1)*base; col++) {
						blockPanel.add(board.contents.get(new Position(base, row, col)).getView());
					}
				}
				
				this.getContentPane().add(blockPanel);
			}
		}
	}

	// http://mapleta.maths.uwa.edu.au/~gordon/sudokumin.php
	static Integer[][] start = {
			{ null, null, null, null, null, null, null, 1, null },
			{ 4, null, null, null, null, null, null, null, null },
			{ null, 2, null, null, null, null, null, null, null },

			{ null, null, null, null, 5, null, 4, null, 7 },
			{ null, null, 8, null, null, null, 3, null, null },
			{ null, null, 1, null, 9, null, null, null, null },

			{ 3, null, null, 4, null, null, 2, null, null },
			{ null, 5, null, 1, null, null, null, null, null },
			{ null, null, null, 8, null, 6, null, null, null } };
}
