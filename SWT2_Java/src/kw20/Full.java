package kw20;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;


public class Full implements Value {
	private final int fill;

	public Full(int fill) {
		this.fill = fill;
	}

	public int getFill() {
		return fill;
	}
	
	public void update(JComponent view, Board board, Cell cell) {
		final JLabel label = new JLabel();
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setVerticalAlignment(JLabel.CENTER);
		label.setText(Integer.toString(fill));

		view.removeAll();
		view.setLayout(new BorderLayout());
		view.add(label,BorderLayout.CENTER);
		view.validate();
	}

	public void removeOption(int fill) {
	}
}
