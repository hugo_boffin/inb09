package kw18;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class MVC extends JApplet {
	static class Counter extends Observable {
		private int state = 0;
		public void step() {
			this.state++;
			this.setChanged(); 
	        this.notifyObservers();
		}
		public int getState() {
			return this.state;
		}
	}
	
	static class Watch implements Observer {
		final JLabel lab = new JLabel();
		public Component view() {
			return this.lab;
		}		
		public void update(Observable arg0, Object arg1) {
			Counter model = (Counter)arg0;
			
			lab.setText(Integer.toString(model.getState()));
			//System.out.println("Step -> "+model.getState());
		}
	}
	
	public void init() {
		final Counter model = new Counter();
		Watch w = new Watch();
		model.addObserver(w);

		JButton but = new JButton("but");
		but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.step();
			}
		});
		
		this.getContentPane().setLayout( new GridLayout(0,1) );
		this.getContentPane().add(but);
		this.getContentPane().add(w.view());
	}
}
