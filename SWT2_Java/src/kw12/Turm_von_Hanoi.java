package kw12;
public class Turm_von_Hanoi {
	static int zug = 0;

	static void turm(int anzahl, char vonturm, char nachturm, char lager) {
		if (anzahl > 0) {
			turm(anzahl - 1, vonturm, lager, nachturm);
			System.out.print("( "+ vonturm + " , " + nachturm+" ) ,");
			zug++;
			turm(anzahl - 1, lager, nachturm, vonturm);
		}

	}

	public static void main(String args[]) {
		char quelle = 'A', ziel = 'B', hilfe = 'C';
		int scheiben = 8;
		turm(scheiben, quelle, ziel, hilfe);
		System.out.println();
		System.out.println(zug + " Zuege");
	}
}
