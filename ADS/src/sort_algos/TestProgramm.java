package sort_algos;

/*
 * 09INB2 Nimmerland
 */
import java.text.DecimalFormat;
import java.util.Random;

public class TestProgramm {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final int durchlaeufe = 3;
		final int wortlaenge = 15;
		final int maxElements = 5000;
		final int step = 100;

		Schluessel[] valPool = new Schluessel[maxElements];
		FuelleArrayZufaellig(valPool, wortlaenge);

		DecimalFormat fmt = new DecimalFormat("0.00000");

		SortierAlgorithmus algos[] = { new BubbleSort(), new InsertionSort(),
				new ShakerSort(), new BubVarSort() };

		System.out.println("Sort	DataType	NumElements	Time");

		for (int i = 0; i < algos.length; i++) {
			for (int e = step; e <= maxElements; e += step) {
				Schluessel[] a = new Schluessel[e];
				System.arraycopy(valPool, 0, a, 0, e);

				System.out.println(algos[i].getClass().getName()
						+ "	Zufaellig	" + e + "	"
						+ fmt.format(Messe(algos[i], a, durchlaeufe)));

				algos[1].Sortiere(a);

				System.out
						.println(algos[i].getClass().getName() + "	Steigend	"
								+ e + "	"
								+ fmt.format(Messe(algos[i], a, durchlaeufe)));

				Umkehren(a);

				System.out
						.println(algos[i].getClass().getName() + "	Fallend	"
								+ e + "	"
								+ fmt.format(Messe(algos[i], a, durchlaeufe)));
			}
		}
	}

	private static void FuelleArrayZufaellig(Schluessel[] a, int wertLaenge) {
		Random rand = new Random( System.currentTimeMillis() );
		String s;

		for (int i = 0; i < a.length; i++) {
			s = "";
			for (int j = 0; j < wertLaenge; j++)
				s += rand.nextInt(3) + 1;
			a[i] = new Schluessel(s);
		}
	}

	private static double Messe(SortierAlgorithmus sortierer,
			Schluessel[] schluessel, int durchlaeufe) {
		double ergebniss = 0.0;
		long vorher;
		long nachher;
		Schluessel[] a;

		for (int d = 0; d < durchlaeufe; d++) {
			a = schluessel.clone();

			vorher = System.currentTimeMillis();
			//vorher = System.nanoTime();
			sortierer.Sortiere(a);
			//nachher = System.nanoTime();
			nachher = System.currentTimeMillis();

			ergebniss += ((nachher - vorher) / 1000.0);
			//ergebniss += ((nachher - vorher) / 1000000000.0);

			// DEBUG - Nachschauen ob Array aufsteigend sortiert
			/*for (int x = 0; x < a.length - 1; x++)
				if (Schluessel.Vergleiche(a[x], a[x + 1]) > 0)
					System.out.println("Schei�e");*/
		}
		return ergebniss / (double) durchlaeufe;
	}

	private static void Umkehren(Schluessel a[]) {
		int n = a.length;
		Schluessel h;
		for (int i = 0; i < n / 2; i++) {
			h = a[n - i - 1];
			a[n - i - 1] = a[i];
			a[i] = h;
		}
	}
}