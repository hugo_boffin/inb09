package sort_algos;

/*
 * 09INB2 Nimmerland
 */
public class BubVarSort implements SortierAlgorithmus {

	public void Sortiere(Schluessel[] a) {
		Schluessel cache;
		boolean warSortiert = true;
		int k = 1, i = 1;

		do {
			warSortiert = true;
			cache = a[i];
			k = i;
			while (k > 0 && Schluessel.Vergleiche(a[k - 1], cache) > 0) {
				warSortiert = false;
				a[k] = a[k - 1];
				k--;
			}

			if (k != a.length - 1)
				warSortiert = false;

			a[k] = cache;

			i++;
			if (i >= a.length) {
				i = 1;
			}

		} while (!warSortiert);
	}
}
