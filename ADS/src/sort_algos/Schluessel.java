package sort_algos;

/*
 * 09INB2 Nimmerland
 */
public class Schluessel {
	/*
	 * String ist final, deswegen Wrapper-Klasse
	 */
	public Schluessel() {
	}

	public Schluessel(String s) {
		val = s;
	}

	public Schluessel(int i) {
		val = new Integer(i).toString();
	}

	/*
	 * A > B : 1 A = B : 0 A < B : -1
	 */
	public static int Vergleiche(Schluessel a, Schluessel b) {
		if (a.length() == b.length()) {
			for (int i = a.length() - 1; i >= 0; i--) {
				if (a.charAt(i) > b.charAt(i))
					return 1;
				if (a.charAt(i) < b.charAt(i))
					return -1;
			}
			return 0;
		} else {
			return (a.length() > b.length()) ? 1 : -1;
		}
	}

	public void set(String s) {
		val = s;
	}

	public int length() {
		return val.length();
	}

	public char charAt(int i) {
		return val.charAt(i);
	}

	public String toString() {
		return val;
	}

	private String val;
}
