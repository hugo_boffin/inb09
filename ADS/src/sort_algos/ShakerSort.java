package sort_algos;

/*
 * 09INB2 Nimmerland
 */
public class ShakerSort implements SortierAlgorithmus {
	private void Vertausche(Schluessel[] a, int i, int j) {
		final Schluessel h = a[i];
		a[i] = a[j];
		a[j] = h;
	}

	public void Sortiere(Schluessel[] a) {
		boolean warSortiert;
		int links = 0;
		int rechts = a.length - 2;
		int fertig = rechts;
		do {
			warSortiert = true;
			for (int i = links; i <= rechts; i++) { // Links -> Rechts
				if (Schluessel.Vergleiche(a[i], a[i + 1]) > 0) {
					warSortiert = false;
					fertig = i;
					Vertausche(a, i, i + 1);
				}
			}
			rechts = fertig - 1;
			for (int i = rechts; i >= links; i--) { // Rechts -> Links
				if (Schluessel.Vergleiche(a[i], a[i + 1]) > 0) {
					warSortiert = false;
					fertig = i;
					Vertausche(a, i, i + 1);
				}
			}
			links = fertig + 1;
		} while (!warSortiert);
	}

}
