package sort_algos;

/*
 * 09INB2 Nimmerland
 */
public class InsertionSort implements SortierAlgorithmus {

	public void Sortiere(Schluessel[] a) {
		Schluessel temp;
		int k;
		for (int i = 1; i < a.length; i++) {
			temp = a[i];
			k = i;
			while (k > 0 && Schluessel.Vergleiche(a[k - 1], temp) > 0) {
				a[k] = a[k - 1];
				k--;
			}
			a[k] = temp;
		}
	}

}
