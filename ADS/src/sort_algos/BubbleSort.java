package sort_algos;

/*
 * 09INB2 Nimmerland
 */
public class BubbleSort implements SortierAlgorithmus {
	private void Vertausche(Schluessel[] a, int i, int j) {
		Schluessel h = a[i];
		a[i] = a[j];
		a[j] = h;
	}

	public void Sortiere(Schluessel[] a) {
		boolean warSortiert;
		do {
			warSortiert = true;
			for (int i = 0; i < a.length - 1; i++) {
				if (Schluessel.Vergleiche(a[i], a[i + 1]) > 0) {
					warSortiert = false;
					Vertausche(a, i, i + 1);
				}
			}
		} while (!warSortiert);
	}
}
