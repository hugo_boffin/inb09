package beleg03;

public class BubbleSort {

	/**
	 * @param args
	 */
	//static int a[] = {20,28,31,5,24,39,14,1,15,54}; // Vorlesung
	static int a[] = {4,19,11,2,5,20,1,15}; // Beleg 3 A1
	static int vertauschungen = 0;
	static int vergleiche = 0;
	
	static void vertausche(int a[], int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
		//System.out.println("Tausche "+i+ " und "+j);
		vertauschungen++;
	}
	
	static void bubbleIt(int a[]) {
		boolean warSortiert = false;
		
		while (!warSortiert) {
			warSortiert = true;
			for (int i = 0; i < a.length-1; i++) {
				vergleiche++;
				if (a[i]>a[i+1]) {
					warSortiert = false;
					vertausche(a,i,i+1);
				}	
			}
			display(a);
		}
	}
	static void bubbleIt_optimized(int a[]) {
		boolean warSortiert = false;
		int unsorted = a.length-1;
		
		while (!warSortiert) {
			warSortiert = true;
			for (int i = 0; i < unsorted; i++) {
				vergleiche++;
				if (a[i]>a[i+1]) {
					warSortiert = false;
					vertausche(a,i,i+1);
				}	
			}
			display(a);
			unsorted--;
		}
	}
	
	static void display(int a[]) {
		for (int i = 0; i < a.length; i++)
			System.out.print(a[i]+" ");
		System.out.println();
	}
	public static void main(String[] args) {
		display(a);
		
		bubbleIt_optimized(a);
		
		System.out.println("Vertauschungen: " + vertauschungen);
		System.out.println("Vergleiche: " +vergleiche);
	}

}
