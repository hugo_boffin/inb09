package beleg03;

public class BinaereSuche {

	/**
	 * @param args
	 */
	
	static int a[] =     {1,3,7,18,22,23,25,27,32,58,80};
	static int daten[] = {1,2,3,4, 5, 6, 7, 8, 9, 10,11};
	
	static int binSucheRek(int gesucht, int links, int rechts) {
		System.out.println(links+" "+rechts);
		if (links <= rechts) {
			int mitte = (int)Math.floor((double)(links+rechts)/2.0);
			if (a[mitte] == gesucht)
				return daten[mitte];
			else if (a[mitte] > gesucht)
				return binSucheRek(gesucht,links,mitte-1);
			else if (a[mitte] < gesucht)
				return binSucheRek(gesucht,mitte+1,rechts);
		}
		System.out.println("Not Found");
		return -1;
	}
	
	public static void main(String[] args) {
		System.out.println("->"+binSucheRek(22,0,a.length-1));
	}

}
