package beleg01;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class ZyklusTest {
	static boolean[][] graph = new boolean[6][6];

	static void  setKante(int x, int y, boolean value) {
		graph[x-1][y-1] = value;
		graph[y-1][x-1] = value;
	}
	static ArrayList<String> zyklen;
	
	static void insertZyklus(String weg) {
		// Pr�ft auf doppelte Zyklen (umgekehrt, um ein element verschoben, usw.)
		boolean vorhanden = false;

		//String[] nodes = weg.split(Pattern.quote( " " ));
		//System.out.println( Arrays.toString(nodes) );
		for (int i = 0; i < zyklen.size(); i++) {
			if (zyklen.get(i).length() == weg.length()) {
				String[] test = zyklen.get(i).split(Pattern.quote( " " ));
				for (int j = 0; j < test.length; j++) {
					String ts = "";
					for (int k = 0; k < test.length; k++)
						ts = ts + test[(k+j) % test.length] + " ";
					
					if ( ts.equals(weg) ) {
						vorhanden = true;
						//System.out.println("GLEICH "+ts+","+weg);
					}
				}
				for (int j = 0; j < test.length; j++) {
					String ts = "";
					for (int k = test.length-1; k >= 0; k--)
						ts = ts + test[(k+j) % test.length] + " ";
					
					if ( ts.equals(weg) ) {
						vorhanden = true;
						//System.out.println("GLEICH "+ts+","+weg);
					}
				}
			}
		}
		
		if (!vorhanden) {
			zyklen.add(weg);
			System.out.println(weg);
		}
	}
	
	static void findZyklus(int startNode, int node,String weg, boolean start) {
		if (startNode == node && start == false) { // Zyklus? 
			insertZyklus(weg);
			return;
		}
		weg = weg + (node+1) + " ";
	
		for (int i = 0 ; i < 6; i++) // Zyklus mindestens 3 Knoten&Kanten
			if (graph[node][i] == true && ((startNode == i && weg.length()>4) || !weg.contains((i+1)+"") ) ) // Kante?
				findZyklus(startNode,i,weg,false);
	}
	
	public static void main(String[] args) {
		for (int y = 0; y < 6; y++)
			for (int x = 0; x < 6; x++)
				graph[x][y] = (x == y) ? true : false;
						
		setKante(1,2,true);
		setKante(1,4,true);
		setKante(1,6,true);
		setKante(1,3,true);
		setKante(2,4,true);
		setKante(4,3,true);
		setKante(4,6,true);
		setKante(3,5,true);
		setKante(5,6,true);
		
		zyklen = new ArrayList<String>();
		
		for (int i = 0; i < 6; i++)
			findZyklus(i, i, "", true);
		
		System.out.println(zyklen.size()+" Zyklen");
	}
}
