#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>

/*
0	Abgewiesen
1	Angenommen
*/

int main(int argc, char *argv[]) {
	if (argc<2) {
		printf("Aufruf: client FILENAME\n");
		exit(1);
	}

	int sockd; // Client und Server
	int l, sl, flags=0, rc, *p, fd;
	struct sockaddr_un srvadr, cladr;
	char eab[100];
	char fname[100];
	FILE *fp = NULL;
	struct msghdr mh;
	struct cmsghdr *cmp;
	union {
		struct cmsghdr cm;
		char ctrl[sizeof(struct cmsghdr)+sizeof(int)];
	} ctrlu;
	struct iovec iov[1];

	srvadr.sun_family = AF_UNIX;
	strcpy(srvadr.sun_path, "sockdgram1");

	mh.msg_name = (char*)&srvadr;
	mh.msg_namelen = sizeof(srvadr);
	mh.msg_iov = iov;
	mh.msg_iovlen = 1;
	mh.msg_control = ctrlu.ctrl;
	mh.msg_controllen = sizeof(ctrlu);
	mh.msg_flags = 0;

	strcpy(fname, "sockXXXXXX");
	close( mkstemp(fname) );
	cladr.sun_family = AF_UNIX;
	strcpy(cladr.sun_path, fname);
	unlink(cladr.sun_path);

	sockd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockd < 0) {
		perror("socket");
		exit(1);	
	}

	rc = bind(sockd, (struct sockaddr*)&cladr, sizeof(cladr));
	if (rc < 0) {
		perror("bind");
		exit(1);	
	}

	// Anforderung
	strcpy(fname, argv[1]);
	strcpy(eab, argv[1]);
	sl = sizeof(srvadr);
	if ((l = sendto(sockd, eab, strlen(eab)+1, flags, (struct sockaddr*)&srvadr, sl))<0) {
		perror("sendto");
	}

	if ((l=recv(sockd, eab, sizeof(eab), flags))<0) {
		perror("recv");
	}
	switch (eab[0]) {
	case 0:
		printf("File existiert nicht!\n");
		break;
	case 1:
		printf("File Inhalt: \n");

		// File Descriptor empfangen
		// Datei ausgeben
		// File closen

		iov[0].iov_base = fname;
		iov[0].iov_len = sizeof(fname);

		rc = recvmsg(sockd, &mh, 0);
		printf("Empfang: %d Bytes, File %s\n",rc, fname);
		cmp = CMSG_FIRSTHDR(&mh);
		p = (int*)CMSG_DATA(cmp);
		fd = *p;
		while(read(fd, eab, 1)>0)
			printf("%c", eab[0]);
		close(fd);

		break;

	default:
		printf("Kaputte Quittung\n");
		exit(1);
	}

	close(sockd);
	unlink(cladr.sun_path);

	return 0;
}

