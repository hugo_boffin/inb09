#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>

/*
0	Abgewiesen
1	Angenommen
*/

int ende = 0;

void sigfunc(int signr) {
	if (!ende) {
		printf(" -> Ende!\n");
		ende = 1;
	}
}

int main(int argc, char *argv[]) {
	int sockd; // Client und Server
	int l, sl, flags=0, rc, *p;
	struct sockaddr_un srvadr, cladr;
	char eab[100];
	char fname[100];
	FILE *fp = NULL;

	struct msghdr mh;
	struct cmsghdr *cmp;
	union {
		struct cmsghdr cm;
		char ctrl[sizeof(struct cmsghdr)+sizeof(int)];
	} ctrlu;
	struct iovec iov[1];

	srvadr.sun_family = AF_UNIX;
	strcpy(srvadr.sun_path, "sockdgram1");
	unlink(srvadr.sun_path);

	sockd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockd < 0) {
		perror("socket");
		exit(1);	
	}

	rc = bind(sockd, (struct sockaddr*)&srvadr, sizeof(srvadr));
	if (rc < 0) {
		perror("bind");
		exit(1);	
	}

	sigset(SIGINT, sigfunc);

	while ( !ende ) {	
		sl = sizeof(cladr);
		if ((l = recvfrom(sockd, eab, sizeof(eab), flags, (struct sockaddr*)&cladr, &sl))<0) {
			if (errno == EINTR)
				continue;
			perror("recvfrom");
		}

		printf("%s moechte File %s...",cladr.sun_path, eab);
		strcpy(fname, eab);
		fp = fopen(fname, "r");
		if (fp) {
			printf("Existiert!\n",cladr.sun_path, eab);
			eab[0] = 1;
			if ((l=sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&cladr, sl))<0) {
				if (errno == EINTR)
					continue;
				perror("sendto");
			}

			int fd = fileno(fp);

			mh.msg_name = (char*)&cladr;
			mh.msg_namelen = sizeof(cladr);
			mh.msg_iov = iov;
			mh.msg_iovlen = 1;
			mh.msg_control = ctrlu.ctrl;
			mh.msg_controllen = sizeof(ctrlu);
			mh.msg_flags = 0;

			iov[0].iov_base = fname;
			iov[0].iov_len = strlen(fname)+1;

			cmp = CMSG_FIRSTHDR(&mh);
			cmp->cmsg_len = sizeof(ctrlu);
			cmp->cmsg_level = SOL_SOCKET;
			cmp->cmsg_type = SCM_RIGHTS;
			p = (int*)CMSG_DATA(cmp);
			*p = fd;
			rc = sendmsg(sockd, &mh, 0);
			
			fclose(fp);
		} else {
			printf("Existiert nicht!\n");
			eab[0] = 0;
			if ((l=sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&cladr, sl))<0) {
				if (errno == EINTR)
					continue;
				perror("sendto");
			}
		}
	}

	close(sockd);
	unlink(srvadr.sun_path);
	
	return 0;
}

