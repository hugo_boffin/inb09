#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stropts.h>
#include <signal.h>
#include <fcntl.h>

struct steuer {
	char kdo;
	int pid;
};

int shutdown = 0;

void sigfkt(int snr) {
	if (shutdown)
		exit(5);
	shutdown = 1;
	printf(" -> Neue Verbindungen abweisen!\n");
}

int main(int argc, char *argv[]) {
	char fname[100] = "MASTERPIPE";

	unlink(fname);
	struct stat buf;
	int rc = stat (fname, &buf);
	if (rc < 0) {
		if (errno != ENOENT) {
			perror("stat");
			exit (1);
		}
		puts ("Pipe existiert noch nicht");
		mode_t mode = 0644;
		rc = mkfifo (fname, mode);
		if (rc < 0) {
			perror("mkfifo");
			exit (2);
		} else {
			printf ("Pipe %s wurde angelegt\n", fname);
		}
	} else {
		if ( !S_ISFIFO(buf.st_mode)) {
			printf ("File %s existiert, ist keine Pipe!\n", fname);
			exit (3);
		}
		printf ("Pipe %s existiert bereits\n", fname);
		exit(4);
	}
	
	FILE* pd = fopen(fname,"r");
	if (!pd) {
		perror("fopen");
		return -1;
	}
	
	sigset(SIGINT,  sigfkt);

	int fd = fileno(pd);
	struct strbuf data, ctl;
	char text[100];
	struct steuer ctldata;
	int flags = 0;

	data.maxlen = sizeof(text);
	data.buf = text;

	ctl.maxlen = sizeof(struct steuer);
	ctl.buf = (void*)&ctldata;
	
	char fn[100];
	FILE* fp;
	int n=0;
	while ( !(shutdown && n<=0) ) {
		rc = getmsg(fd, &ctl, &data, &flags);
		
		//printf("%i %i %i\n", shutdown, n, rc );
		//if (n==-1) {printf("\nMIST\n");exit(3);}

		if (rc < 0 ) {
			if (errno==EINTR) { // Interupt, einfach weiter
				continue;
			}

			perror("getmsg()");
			exit(1);
		} else {	
			sprintf(fn, "sender-%d.txt", ctldata.pid);
			
			switch (ctldata.kdo) {
			case 0:
				{
					if (shutdown) {
						printf("%ld: Verbindung abgewiesen!\n",ctldata.pid);
						kill(ctldata.pid, SIGUSR1);
					} else {
						printf("%ld: Neue Verbindung\n",ctldata.pid);

						n++;
						fp = fopen(fn, "w");
						if (!fp) {
							perror("fopen");
							return -1;
						}
						fclose(fp);
					}
				}

				break;
			case 1:
				{
					printf("%ld: %s",ctldata.pid,data.buf);
					fp = fopen(fn, "a");
					if (!fp) {
						perror("fopen");
						return -1;
					}
					fprintf(fp,data.buf);
					fclose(fp);
				}
				break;
			case 2:
				{
					printf("%ld: Auf Wiedersehen\n",ctldata.pid);
					n--;

					printf("%ld: Nachrichten:\n", ctldata.pid);
					fp = fopen(fn, "r");
					if (fp) {
						while(fgets(text, sizeof(text), fp)) {
							printf(text);
						}
						fclose(fp);
					}
					unlink(fn);
				}
				break;
			}
		}
	}
		
	fclose(pd);
	unlink(fname);
	
	return 0;
}

