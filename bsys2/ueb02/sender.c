#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stropts.h>

struct steuer {
	char kdo;
	int pid;
};

void sigfkt(int snr) {
	printf("Verbindung abgewiesen!\n");
	exit(1);
}

int main(int argc, char *argv[]) {
	sigset(SIGUSR1, sigfkt);
	char fname[100] = "MASTERPIPE";

	struct stat buf;
	int rc = stat (fname, &buf);
	if (rc < 0) {
		if (errno != ENOENT) {
			perror("stat");
			exit (1);
		}
		puts ("Pipe existiert noch nicht");
		exit (1);
	} else {
		if ( !S_ISFIFO(buf.st_mode)) {
			printf ("File %s existiert, ist keine Pipe!\n", fname);
			exit (3);
		}
		printf ("Pipe %s existiert bereits\n", fname);
	}
	
	FILE* pd = fopen(fname,"w");
	if (!pd) {
		perror("fopen");
		return -1;
	}

	int fd = fileno(pd);
	struct strbuf data, ctl;
	char text[100];
	struct steuer ctldata;
	int flags = 0;

	data.maxlen = sizeof(text);
	data.buf = text;

	ctl.maxlen = ctl.len = sizeof(struct steuer);
	ctl.buf = (void*)&ctldata;

	ctldata.pid = getpid();
	
	ctldata.kdo = 0;
	rc = putmsg(fd,&ctl,0,flags);
	if (rc < 0) {
		perror("putmsg() #1");
		exit(1);
	}

	ctldata.kdo = 1;
	while ( fgets(data.buf, sizeof(text), stdin) ) {	
		data.len = strlen(text)+1;
		rc = putmsg(fd,&ctl,&data,flags);			
		if (rc < 0) {
			perror("putmsg() #2");
			exit(1);
		}
	}

	ctldata.kdo = 2;
	rc = putmsg(fd,&ctl,0,flags);
	if (rc < 0) {
		perror("putmsg() #3");
		exit(1);
	}
	
	fclose(pd);
	
	return 0;
}

