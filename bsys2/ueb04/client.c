#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

/*
0	Anmeldung
1	Abgewiesen
2	Angenommen
3	Abmeldung
*/

int main(int argc, char *argv[]) {
	int sockd; // Client und Server
	int l, sl, flags=0, rc;
	struct sockaddr_un srvadr, cladr;
	char eab[100];

	srvadr.sun_family = AF_UNIX;
	strcpy(srvadr.sun_path, "sockdgram1");

	char fname[50];
	int p;
	strcpy(fname, "sockXXXXXX");
	p = mkstemp(fname);
	cladr.sun_family = AF_UNIX;
	strcpy(cladr.sun_path, fname);
	unlink(cladr.sun_path);

	sockd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockd < 0) {
		perror("socket");
		exit(1);	
	}

	rc = bind(sockd, (struct sockaddr*)&cladr, sizeof(cladr));
	if (rc < 0) {
		perror("bind");
		exit(1);	
	}

	// Anmelden
	eab[0] = 0;
	sl = sizeof(srvadr);
	if ((l = sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&srvadr, sl))<0) {
		perror("sendto");
	}

	if ((l=recv(sockd, eab, sizeof(eab), flags))<0) {
		perror("recv");
	}
	switch (eab[0]) {
	case 1:
		printf("Abgewiesen!\n");
		exit(0);
	case 2:
		printf("Angenommen!\n");
		break;
	default:
		printf("Falsche Quittung!\n");
		exit(1);
	}

	// Nachrichten
	while ( fgets(eab, sizeof(eab), stdin) ) {	
		sl = sizeof(srvadr);
		if((l = sendto(sockd, eab, strlen(eab)+1, flags, (struct sockaddr*)&srvadr, sl))<0) {
			perror("sendto");
		}
	}

	// Abmelden
	eab[0] = 3;
	sl = sizeof(srvadr);
	l = sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&srvadr, sl);

	close(sockd);
	unlink(cladr.sun_path);

	return 0;
}

