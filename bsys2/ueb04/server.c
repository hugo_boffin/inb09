#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>
#include <unistd.h>

/*
0	Anmeldung
1	Abgewiesen
2	Angenommen
3	Abmeldung
*/

int ende = 0;

void sigfunc(int signr) {
	if (!ende) {
		printf(" -> Ende nachdem alle Clients weg sind!\n");
		ende = 1;
	}
}

int main(int argc, char *argv[]) {
	int clients = 0; // Anzahl Clients
	FILE *fp = NULL;
	char fn[100];
	
	int sockd; // Client und Server
	int l, sl, flags=0, rc;
	struct sockaddr_un srvadr, cladr;
	char eab[100];

	srvadr.sun_family = AF_UNIX;
	strcpy(srvadr.sun_path, "sockdgram1");
	unlink(srvadr.sun_path);

	sockd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sockd < 0) {
		perror("socket");
		exit(1);	
	}

	rc = bind(sockd, (struct sockaddr*)&srvadr, sizeof(srvadr));
	if (rc < 0) {
		perror("bind");
		exit(1);	
	}

	sigset(SIGINT, sigfunc);

	while ( !ende || clients > 0 ) {	
		sl = sizeof(cladr);
		if ((l = recvfrom(sockd, eab, sizeof(eab), flags, (struct sockaddr*)&cladr, &sl))<0) {
			if (errno == EINTR)
				continue;
			perror("recvfrom");
		}

		sprintf(fn,"%s.txt",cladr.sun_path);

		switch (eab[0]) {
		case 0:
			if (ende) {
				eab[0] = 1;
				if ((l=sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&cladr, sl))<0) {
					if (errno == EINTR)
						continue;
					perror("sendto");
				}
			} else {
				fp = fopen(fn, "w");
				fclose(fp);

				eab[0] = 2;
				clients++;
				printf("%s: Angemeldet\n",cladr.sun_path);
				if ((l=sendto(sockd, eab, sizeof(char), flags, (struct sockaddr*)&cladr, sl))<0) {
					if (errno == EINTR)
						continue;
					perror("sendto");
				}
			}
			break;
		case 3:
			printf("%s: Abgemeldet. Nachrichten:\n",cladr.sun_path);
			fp = fopen(fn, "r");
			while (fgets(eab, sizeof(eab), fp)) {
				fputs(eab,stdout);
			}
			fclose(fp);
			clients--;
			unlink(fn);
			break;
		default:
			printf("%s: %s",cladr.sun_path, eab);
			fp = fopen(fn, "a");
			fputs(eab,fp);
			fclose(fp);
		}
	}

	close(sockd);
	unlink(srvadr.sun_path);
	
	return 0;
}

