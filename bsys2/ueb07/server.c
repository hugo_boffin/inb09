#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pthread.h>

int ende = 0;

struct thread_args {
	int sockd2;
};

void* threadFunc(void* p);
void sigfunc(int signr);

int main() {
	const char SOCKNAME[16] = "Stecker";
	const int qsize = 10;
	int thrids[100];
	int i,n = 0;

	pthread_attr_t attr;
	pthread_t thrid;
	struct thread_args *args = NULL;
	int rc;
	pthread_attr_init(&attr);		// Thread-Attribute initialisieren
	int scope = PTHREAD_SCOPE_SYSTEM;	// Threads in Konkurrenz um CPU mit allen Prozessen
	if((rc=pthread_attr_setscope(&attr,scope)) != 0){
		fprintf(stderr,"pthread_attr_setscope: %s\n", strerror(rc));
		exit(1);
	}

	int sockd, sockd2, l, status;
	struct sockaddr_un uxadr;

	uxadr.sun_family = AF_UNIX;
	strcpy(uxadr.sun_path, SOCKNAME);

	if((sockd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}

	unlink(uxadr.sun_path);
	if(bind(sockd, (struct sockaddr*)&uxadr, sizeof(uxadr)) < 0) {
		perror("bind");
		exit(1);
	}

	if(listen(sockd, qsize) < 0) {
		perror("listen");
		exit(1);
	}

	signal(SIGINT, sigfunc);
	printf("Listening...\n");
	do {
		if((sockd2 = accept(sockd, 0, 0)) < 0) {
			if (errno == EINTR) {
				continue;
			}
			perror("accept");
			exit(1);
		}

		if (ende)
			break;

		//printf("XXX\n");

		args = (struct thread_args *)malloc(sizeof(struct thread_args));
		args->sockd2 = sockd2;

		if((rc=pthread_create(&thrid,&attr,threadFunc,args)) != 0){
			fprintf(stderr,"pthread_create: %s\n", strerror(rc));
			exit(1);
		}

		// ThreadID in Array einfuegen
		thrids[n++] = thrid;
	} while(!ende);
	

	if(close(sockd) < 0) {
		perror("close(sockd)");
		exit(1);
	}

	printf("Warten...\n");
	// Threadenden abwarten
	for (i = 0; i < n; i++) {
		printf("Thread %d/%d...\n",i+1,n);
		rc = pthread_join(thrids[i],NULL);
		if (rc != 0) {
			fprintf(stderr,"pthread_join: %s\n", strerror(rc));
		}
	}
	

	unlink(uxadr.sun_path);

	return 0;
}

void* threadFunc(void* p) {
	sigset_t neu;
	sigemptyset(&neu);
	sigaddset(&neu,SIGINT);
	pthread_sigmask(SIG_BLOCK,&neu,NULL);

	FILE *fp = NULL;
	char eab[100], fileName[100];
	int l, thrid = pthread_self();
	int sockd2 = ((struct thread_args*)p)->sockd2;

	printf("%d: Neuer Client\n",thrid);
	if((l = recv(sockd2, eab, sizeof(eab), 0)) < 0) {
		perror("recv");
	}

	strcpy(fileName, eab);

	if((fp = fopen(eab, "w")) < 0) {
		perror("fopen");
		exit(1);
	}
	printf("%d: Nach File '%s'\n", thrid, eab);

	do {
		if((l = recv(sockd2, eab, sizeof(eab), 0)) < 0) {
			perror("recv");
		}

		if(l>0) {
			printf("%d: %d Byte: %s", thrid, l, eab);

			if(fwrite(&eab, strlen(eab)+1, 1, fp) < 0) {
				perror("fwrite");
				exit(1);
			}

			sprintf(eab, "%d", l);
			if((l=send(sockd2, eab, strlen(eab)+1, 0)) < 0) {
				perror("send");
			}
		}
	} while(l != 0);
	printf("%d: Gute Nacht\n",thrid);
	fclose(fp);

	if (close(sockd2) < 0) {
		perror("close(sockd2)");
		exit(1);
	}

	return NULL;
}

void sigfunc(int signr) {
	printf(" -> Ende nachdem alle Clients weg sind!\n");
	ende = 1;
}
