#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/un.h>

int main(int argc, char** argv) {
	const char SOCKNAME[16] = "Stecker";
	char eab[100];
	char fileName[100];
	char action[10];
	FILE* fp;
	struct sockaddr_un uxadr;
	int sockd, l;

	uxadr.sun_family = AF_UNIX;
	strcpy(uxadr.sun_path, SOCKNAME);

	if((sockd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}	

	if(connect(sockd, (struct sockaddr*)&uxadr, sizeof(uxadr)) < 0) {
		perror("connect");
		exit(1);
	}

	if (argc==2) {
		strcpy(fileName,argv[1]);
	} else if(argc==1) {
		printf("File: ");
		gets(fileName);
	} else {
		printf("Ungültige Argumente!\n");
		exit(1);
	}

	if((fp = fopen(fileName, "r")) == NULL) {
		if(errno!=ENOENT) {
			perror("fopen");
			exit(1);
		}
	} else {
		fclose(fp);
		printf("File \"%s\" existiert. Überschreiben? (j/n): ", fileName);
		gets(action);

		if(toupper(action[0])=='J') {
			if((fp = fopen(fileName, "w")) == NULL) {
				perror("fopen");
				exit(1);
			} else {
				fclose(fp);
			}
		} else {
			printf("Ende...\n");
			exit(1);
		}
	}

	if((l = send(sockd, fileName, strlen(fileName)+1, 0)) < 0) {
		perror("send");
		exit(1);
	}

	printf("Verbunden!\n");
	while((fgets(eab, sizeof(eab), stdin)) != NULL) {
		if((l = send(sockd, eab, strlen(eab)+1, 0)) < 0) {
			perror("send");
			exit(1);
		}
		if(recv(sockd, eab, sizeof(eab), 0) < 0) {
			perror("recv");
			exit(1);
		}
		printf("%d Byte raus, %s Byte quittiert\n",l,eab);
	}

	if(close(sockd) < 0) {
		perror("close");
		exit(1);
	}
	
	return 0;
}
