#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>

// Signal ignorieren im child

int ende = 0;
pid_t ppid;

void sigfunc(int signr) {
	if (!ende && getpid() == ppid) {
		printf(" -> Ende nachdem alle Clients weg sind!\n");
		ende = 1;
	}
}

int main(void) {
	const char SOCKNAME[16] = "Stecker";
	const int qsize = 10;

	int sockd, sockd2, l, status;
	pid_t pid, pid2;
	struct sockaddr_un uxadr;
	char eab[100], fileName[100];
	FILE *fp = NULL;

	ppid = getpid();

	uxadr.sun_family = AF_UNIX;
	strcpy(uxadr.sun_path, SOCKNAME);

	if((sockd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		perror("socket");
		exit(1);
	}

	unlink(uxadr.sun_path);
	if(bind(sockd, (struct sockaddr*)&uxadr, sizeof(uxadr)) < 0) {
		perror("bind");
		exit(1);
	}

	if(listen(sockd, qsize) < 0) {
		perror("listen");
		exit(1);
	}

	sigset(SIGINT, sigfunc);
	printf("Listening...\n");
	do{
		if((sockd2 = accept(sockd, 0, 0)) < 0 && !ende) {
			perror("accept");
			exit(1);
		}

		if(ende)
			break;

		pid = fork();
		switch(pid) {
			case -1:
				perror("fork");
				exit( 1 );
			case 0:
				pid2 = (int)getpid();
				printf("%d: Neuer Client\n", pid2);
				if(close(sockd) < 0) {
					perror("close");
					exit(1);
				}

				if((l = recv(sockd2, eab, 100, 0)) < 0) {
					perror("recv");
				}

				strcpy(fileName, eab);

				if((fp = fopen(eab, "w")) < 0) {
					perror("fopen");
					exit(1);
				}
				printf("%d: Nach File '%s'\n", pid2, eab);
				
				do {
					if((l = recv(sockd2, eab, 100, 0)) < 0) {
						if (errno == EINTR)
							continue;
						perror("recv");
					}

					if(l>0) {
						printf("%d: %d Byte: %s", pid2, l, eab);

						if(fwrite(&eab, strlen(eab)+1, 1, fp) < 0) {
							perror("fwrite");
							exit(1);
						}

						sprintf(eab, "%d", l);
						if((l=send(sockd2, eab, strlen(eab)+1, 0)) < 0) {
							perror("send");
						}
					}
				} while(l != 0);
				printf("%d: Gute Nacht\n", pid2);
				fclose(fp);
				
				if (close(sockd2) < 0) {
					perror("close(sockd2)");
					exit(1);
				}
				
				exit(1);
				break;
			default:
				if(close(sockd2) < 0) {
					perror("close(sockd2)");
					exit(1);
				}
				break;
		}
	} while(!ende);

	if(close(sockd) < 0) {
		perror("close(sockd)");
		exit(1);
	}
	
	while ((pid=wait(&status)) > 0) {
    		printf("Child %d Ende mit Status %x %x\n", (int)pid, (status>>8)&0xFF, status&0xFF);
	}

	unlink(uxadr.sun_path);

	return 0;
}
