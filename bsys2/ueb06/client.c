#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/un.h>
#include <sys/shm.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/sem.h> 

#include "global.h"

int shmid,shmflag,shmsize,opflag;
key_t shmkey;
struct shm_buffer *buf;

int semid,nsems,semflag;
key_t key;

int setmsg(struct shm_buffer *newbuf) {
	int result = 0;

	struct sembuf lock[2],unlock;
	lock[0].sem_num = 0;
	lock[0].sem_op = 0;
	lock[0].sem_flg = 0;
	lock[1].sem_num = 0;
	lock[1].sem_op = 1;
	lock[1].sem_flg = SEM_UNDO;
	unlock.sem_num = 0;
	unlock.sem_op = -1;
	unlock.sem_flg = IPC_NOWAIT | SEM_UNDO;

	// P
	//printf("P()\n");
	if (semop(semid,lock,2) < 0) {
		perror("sem_op()");
		exit(1);
	}

	if (buf->flag == LEERLAUF) {
		result = 1;
		buf->pid = newbuf->pid;
		buf->flag = newbuf->flag;
		strcpy(buf->text, newbuf->text);
	}

	// V
	//printf("V()\n");
	if (semop(semid,&unlock,1) < 0) {
		perror("sem_op()");
		exit(1);
	}
	return result;
}

int main(int argc, char *argv[]) {
	int rc;
	// Shared Memory
	shmsize = sizeof(struct shm_buffer);
	shmkey = ftok("shmfile", 1);
	shmflag = IPC_CREAT | 0644;
	shmid = shmget(shmkey, shmsize, shmflag);
	if (shmid < 0) {
		perror("shmkey()");
		exit(1);
	}
	
	// Zugriff vorbereiten;
	buf = (struct shm_buffer *)shmat(shmid,0,0);

	// Semaphore-Menge
	nsems = 1;
	key = ftok("semfile", 1);
	semflag = IPC_CREAT | 0644;
	semid = semget(key,nsems, semflag);
	if (semid < 0) {
		perror("semget()");
		exit(1);
	}

	struct shm_buffer newbuf;
	newbuf.flag = ANMELDUNG;
	newbuf.pid = getpid();
	
	while (!setmsg(&newbuf))
		usleep(1000000);

	newbuf.flag = NACHRICHT;
	while ( fgets(newbuf.text, sizeof(newbuf.text), stdin) ) {
		while (!setmsg(&newbuf))
			usleep(1000000);
	}

	newbuf.flag = ABMELDUNG;
	while (!setmsg(&newbuf))
		usleep(1000000);

	// Zuordnung aufheben
	shmdt((char*)buf);

	return 0;
}

