#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/un.h>
#include <sys/shm.h>
#include <sys/types.h> 
#include <sys/ipc.h>
#include <sys/sem.h> 

#include "global.h"

int ende = 0;
int clients = 0;

void sigfunc(int signr) {
	if (!ende) {
		printf(" -> Ende sobald alle Clients weg!\n");
		ende = 1;
	}
}

int main(int argc, char *argv[]) {
	int rc;
	// Shared Memory
	int shmid,shmflag,shmsize,opflag;
	key_t shmkey;
	shmsize = sizeof(struct shm_buffer);
	shmkey = ftok("shmfile", 1);
	shmflag = IPC_CREAT | 0644;
	shmid = shmget(shmkey, shmsize, shmflag);
	if (shmid < 0) {
		perror("shmkey()");
		exit(1);
	}
	
	// Zugriff vorbereiten
	struct shm_buffer *buf = (struct shm_buffer *)shmat(shmid,0,0);
	buf->flag = LEERLAUF;

	// Semaphore-Menge
	int semid,nsems,semflag;
	key_t key;
	nsems = 1;
	key = ftok("semfile", 1);
	semflag = IPC_CREAT | 0644;
	semid = semget(key,nsems, semflag);
	if (semid < 0) {
		perror("semget()");
		exit(1);
	}

	union semun {
		int val;
		struct semid_ds *buf;
		ushort *array;
	} semvalue;

	semvalue.val = 0;
	rc = semctl(semid,0,SETVAL,semvalue);
	if (semid < 0) {
		perror("semctl()");
		exit(1);
	}

	sigset(SIGINT, sigfunc);
	while ( !ende || clients > 0 ) {
		struct sembuf lock[2],unlock;
		lock[0].sem_num = 0;
		lock[0].sem_op = 0;
		lock[0].sem_flg = 0;
		lock[1].sem_num = 0;
		lock[1].sem_op = 1;
		lock[1].sem_flg = SEM_UNDO;
		unlock.sem_num = 0;
		unlock.sem_op = -1;
		unlock.sem_flg = IPC_NOWAIT | SEM_UNDO;
		
		// P
		//printf("P()\n");
		if (semop(semid,lock,2) < 0) {
			if (errno == EINTR)
				continue;
			perror("sem_op()");
			exit(1);
		}

		// Befehl enthalten ? Verarbeiten
		switch (buf->flag) {
		case LEERLAUF:
			//printf("LEERLAUF\n");
			break;
		case ANMELDUNG:
			printf("%d: ANMELDUNG\n", buf->pid);
			clients++;
			buf->flag = LEERLAUF;
			break;
		case NACHRICHT:
			printf("NACHRICHT\n");
			printf("%d: %s", buf->pid, buf->text);
			buf->flag = LEERLAUF;
			break;
		case ABMELDUNG:
			printf("%d: ABMELDUNG\n", buf->pid);
			clients--;
			buf->flag = LEERLAUF;
			break;
		default:
			break;
		}

		// V
		//printf("V()\n");
		if (semop(semid,&unlock,1) < 0) {
			perror("sem_op()");
			exit(1);
		}

		usleep(100000);
	}

	// Zuordnung aufheben
	shmdt((char*)buf);

	// Semaphor-Menge entfernen
	semctl(semid,0,IPC_RMID,0);

	// SHM aus System entfernen
	shmctl(shmid, IPC_RMID, 0);

	return 0;
}

