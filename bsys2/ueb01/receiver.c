#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

struct msgtyp {
	long mtype;
	char mtext[100];
}; 

int main(int argc, char *argv[]) {
	char fname[100];
	FILE* fp;
	int n = 0;
	int ende = 0;

	struct msgtyp msg;
	size_t len = sizeof(msg.mtext);
	int l, rc, flag=0;
	long mtyp = 0;

	int msqid;
	int msqflag = IPC_CREAT|0644;
	key_t msqkey = 47110815;
	
	msqid = msgget (msqkey, msqflag);
	if (msqid<0) {
		perror("msgget");
		exit(1);
	}

	while (!ende) {
		l = msgrcv (msqid, &msg, len, mtyp, flag);

		sprintf(fname, "sender-%d.txt", msg.mtype);

		switch (msg.mtext[0]) {
		case 1:
			printf("%d: Neuer Sender\n", msg.mtype);
			n++;
		break;
		case 2:
			printf("%d: Sender weg\n", msg.mtype);
			//unlink(fname);
			n--;
			if (n==0)
				ende = 1;
		break;
		default:
			printf("%d: %s", msg.mtype, msg.mtext);
			fp = fopen(fname, "a");
			fprintf(fp,msg.mtext);
			fclose(fp);
		break;
		}
	}

	if ((rc = msgctl (msqid, IPC_RMID, 0) ) < 0)
		perror("msgctl / Queue entfernen");

	return 0;
}
