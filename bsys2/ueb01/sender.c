#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>

struct msgtyp {
	long mtype;
	char mtext[100];
};

int main(int argc, char *argv[]) {
	int msqid;
	int msqflag = IPC_CREAT|0644;
	key_t msqkey = 47110815;

	struct msgtyp msg;
	int rc, flag;
	long mtyp;

	msqid = msgget (msqkey, msqflag);
	if (msqid<0) {
		perror("msgget");
		exit(1);
	}

	msg.mtype = getpid();

	msg.mtext[0] = 1;
	if((rc=msgsnd(msqid, &msg, sizeof(char), flag)) < 0) {
		perror("msgsnd #1");
		exit(1);
	}	

	while ( fgets(msg.mtext, sizeof(msg.mtext), stdin) ) {				
		if((rc=msgsnd(msqid, &msg, strlen(msg.mtext)+1, flag)) < 0) {
			perror("msgsnd #2");
			exit(1);
		}	
	}

	msg.mtext[0] = 2;
	if((rc=msgsnd(msqid, &msg, sizeof(char), flag)) < 0) {
		perror("msgsnd #3");
		exit(1);
	}	

	return 0;
}
