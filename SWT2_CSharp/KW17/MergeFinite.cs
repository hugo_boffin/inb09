using System;
using System.Collections.Generic;
using System.Linq;

namespace KW17
{
	public class MergeFinite
	{
		public static IEnumerable<E> Merge<E> (IEnumerable<E> xs, IEnumerable<E> ys) where E : IComparable<E> {
			IEnumerator<E> xit = xs.GetEnumerator();
			IEnumerator<E> yit = ys.GetEnumerator();
			
			bool xnotempty = xit.MoveNext();
			bool ynotempty = yit.MoveNext();

			while (xnotempty || ynotempty) {
				// Am Ende ist immer eine Seite leer - Compare wuerde fehlschlagen!
				if (!xnotempty) {
					yield return yit.Current;
					ynotempty = yit.MoveNext();
				} else if (!ynotempty) {
					yield return xit.Current;
					xnotempty = xit.MoveNext();
				} else { // "Normaler" Vergleich
					int cmp = xit.Current.CompareTo(yit.Current);
					if (cmp <= 0) {
						yield return xit.Current;
						xnotempty = xit.MoveNext();
					} else {
						yield return yit.Current;
						ynotempty = yit.MoveNext();
					}
				}
			}
		}
				
		public static IEnumerable<E> Sort<E>(IEnumerable<E> xs) where E : IComparable<E> {
			if (xs.Count() <= 1) {
	            return xs;
	        } else {
				return Merge(Sort(xs.Take(xs.Count()/2)),Sort(xs.Skip(xs.Count()/2)));
	        }
		}
		
		
		
		public static IEnumerable<E> Merge<E> (IEnumerable<E> xs, IEnumerable<E> ys, IComparer<E> c) {
			IEnumerator<E> xit = xs.GetEnumerator();
			IEnumerator<E> yit = ys.GetEnumerator();
			
			bool xnotempty = xit.MoveNext();
			bool ynotempty = yit.MoveNext();

			while (xnotempty || ynotempty) {
				// Am Ende ist immer eine Seite leer - Compare wuerde fehlschlagen!
				if (!xnotempty) {
					yield return yit.Current;
					ynotempty = yit.MoveNext();
				} else if (!ynotempty) {
					yield return xit.Current;
					xnotempty = xit.MoveNext();
				} else { // "Normaler" Vergleich
					int cmp = c.Compare(xit.Current,yit.Current);
					if (cmp <= 0) {
						yield return xit.Current;
						xnotempty = xit.MoveNext();
					} else {
						yield return yit.Current;
						ynotempty = yit.MoveNext();
					}
				}
			}
		}
		
		public static IEnumerable<E> Sort<E>(IEnumerable<E> xs, IComparer<E> c) {
			if (xs.Count() <= 1) {
	            return xs;
	        } else {
				return Merge(Sort(xs.Take(xs.Count()/2),c),Sort(xs.Skip(xs.Count()/2),c),c);
	        }
		}
		
		
		class Flip<E> : IComparer<E>
		{
			private IComparer<E> boxedComparer;

			public Flip(IComparer<E> boxedComparer)
			{
				this.boxedComparer = boxedComparer;	
			}
		    
			public int Compare(E x, E y)
		    {		
		        return boxedComparer.Compare(y,x);
		    }
		}
		
		class IntComparer : IComparer<int>
		{
		    public int Compare(int x, int y)
		    {		
		        return (x > y) ? 1 : -1;
		    }
		}
		
		public static void runDemo2() {				
			IComparer<int> c = new Flip<int>( new IntComparer() );
			
			// Merge(Endlich) Test
			/*int[] x = new int[] {4,3,1};
			int[] y = new int[] {8,7,2};
						
			foreach (int i in Merge(x,y,c) ) {
				Console.WriteLine (i);
			}*/
						
			// MergeSort Test
			
			Console.WriteLine ();
						
			int[] u = new int[]{1,2,3,4,5,6,7,8,9};
			
			foreach (int i in Sort(u,c) ) {
				Console.WriteLine (i);
			}
		}
		
		public static void runDemo() {			
			// Merge(Endlich) Test
			int[] x = new int[] {1,3,4};
			int[] y = new int[] {2,7,8};
						
			foreach (int i in Merge(x,y) ) {
				Console.WriteLine (i);
			}
						
			// MergeSort Test
			
			Console.WriteLine ();
						
			int[] u = new int[]{9,8,7,6,3,5,4,2,1};
			
			foreach (int i in Sort(u) ) {
				Console.WriteLine (i);
			}
		}
	}
}
