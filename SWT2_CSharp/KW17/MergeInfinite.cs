using System;
using System.Collections.Generic;
using System.Linq;

namespace KW17
{
	public class MergeInfinite
	{
		//Ausgabe: monoton geordnete permutation der verkettung der eingaben
		//Ergänzen: Einige typen, deklarationen, if(...)
		//Take und Skip zum aufteilen im endlichen Fall (MergeSort)
		
		public static IEnumerable<E> Merge<E> (IEnumerable<E> xs, IEnumerable<E> ys) where E : IComparable<E> {
			IEnumerator<E> xit = xs.GetEnumerator();
			IEnumerator<E> yit = ys.GetEnumerator();
			
			xit.MoveNext();
			yit.MoveNext();

			while (true) {
				int cmp = xit.Current.CompareTo(yit.Current);
				if (cmp <= 0) {
					Console.Write("X...");
					yield return xit.Current;
					xit.MoveNext();
				} else {
					Console.Write("Y...");
					yield return yit.Current;
					yit.MoveNext();
				}
			}
		}
				
		public static void runDemo() {
			foreach (int x in Merge(Seminar.Nats().Select(x => x*x),
			                        Seminar.Nats().Select(x => 3*x+1)).Take(10) ) {
				Console.WriteLine (x);
			}
		}
		
	}
}

