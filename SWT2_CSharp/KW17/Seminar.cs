using System;
using System.Collections.Generic;
using System.Linq;

namespace KW17
{
	public class Seminar
	{
		public static IEnumerable<int> Nats () {
		    for (int s = 0; true; s++) {
        		yield return s;
    		}
		}
		
		public static void run ()
		{
			foreach (int x in Enumerable.Range(10,3) ) {
				Console.WriteLine (x);
			}

			Console.WriteLine();
			
			IEnumerator<int> it = Enumerable.Range(10,3).GetEnumerator();
			while ( it.MoveNext() ) {
				Console.WriteLine(it.Current);
			}

			Console.WriteLine();
			
			foreach (int x in Nats().Skip(10).Take(3) ) {
				Console.WriteLine (x);
			}
			
			Console.WriteLine();
			
			IEnumerator<int> it2 = Nats().Select(x => x*x).Skip(10).Take(3).GetEnumerator();
			while ( it2.MoveNext() ) {
				Console.WriteLine(it2.Current);
			}
		}
	}
}

