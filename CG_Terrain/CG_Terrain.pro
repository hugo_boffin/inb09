#-------------------------------------------------
#
# Project created by QtCreator 2011-04-10T17:11:39
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = CG_Terrain
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    terrainwidget.cpp \
    GLee.c

HEADERS  += mainwindow.h \
    terrainwidget.h \
    GLee.h

FORMS    += mainwindow.ui
