#ifndef TERRAINWIDGET_H
#define TERRAINWIDGET_H

#include <QGLWidget>
#include <QObject>
#include <QColor>
#include <vector>

class TerrainWidget : public QGLWidget
{
    Q_OBJECT

public slots:
    void reset();
    void divide();
    void setWireFrame(bool checked);

public:
    TerrainWidget(QWidget *parent = 0);
    ~TerrainWidget();
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    int xRot;
    int yRot;
    int zRot;
    float distance;
    QPoint lastPos;
    QColor clearColor;

    void drawVertex(int x, int y ) const;
    float GetInterpolatedHeight(const QVector2D& uv) const;
    QVector3D GetNormal(const QVector2D& uv) const;
    void generateNormals();

    bool wireframe;
    int size;
    float factor;
    float scale;
    std::vector<QVector3D> normalMap;
    std::vector<float> heightMap;
};

#endif // TERRAINWIDGET_H
