
#include <GLee.h>

#include "terrainwidget.h"

#include <QtGui>
#include <QtOpenGL>
#include <math.h>
#include <QDebug>
#include <QGLWidget>
#include <QMatrix4x4>
#include <QVector3D>
#include <qmath.h>
#include <time.h>
#include <float.h>

TerrainWidget::TerrainWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    xRot = 0;
    yRot = 0;
    zRot = 0;
    clearColor = QColor::fromRgbF(0.15,0.15,0.5,1);
    distance = 2.0f;
    reset();
    wireframe = false;
}

TerrainWidget::~TerrainWidget()
{
}

void TerrainWidget::initializeGL()
{
    qglClearColor(clearColor);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glShadeModel(GL_SMOOTH);

    glEnable(GL_MULTISAMPLE);
    GLint bufs;
    GLint samples;
    glGetIntegerv(GL_SAMPLE_BUFFERS, &bufs);
    glGetIntegerv(GL_SAMPLES, &samples);
    GLfloat LightAmbient[]= { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat LightDiffuse[]= { 1.0f, 1.0f, 1.0f, 1.0f };
    //GLfloat LightSpecular[]= { 1.0f, 1.0f, 1.0f, 1.0f };
    GLfloat LightPosition[]= { 0.0f, 0.0f, 10.0f, 1.0f };

    glEnable(GL_NORMALIZE);
    glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
    glLightfv(GL_LIGHT1, GL_POSITION,LightPosition);
    //glLightfv(GL_LIGHT1, GL_SPECULAR,LightSpecular);

    GLfloat mat_diffuse[] = { 0.1, 0.8, 0.2, 1.0 };
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);
}

void TerrainWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glTranslatef(0, 0, -distance);
    glRotatef(xRot / 16.0, 1.0, 0.0, 0.0);
    glRotatef(yRot / 16.0, 0.0, 1.0, 0.0);
    glRotatef(zRot / 16.0, 0.0, 0.0, 1.0);
    glTranslatef(-0.5, -0.5, 0);

    glScalef(1.0f/(float)(size),1.0f/(float)(size),1.0f/50.0f);

    if (wireframe)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    for (int y = 0; y < size; y++) {
        for (int x = 0; x < size; x++) {
            glBegin(GL_TRIANGLE_STRIP);
                drawVertex(x, y);
                drawVertex(x+1, y);
                drawVertex(x, y+1);
                drawVertex(x+1, y+1);
            glEnd();
       }
    }
}

void TerrainWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0f, (float)width / (float)height, 0.01f, 50.0f);
    glMatrixMode(GL_MODELVIEW);

}

void TerrainWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void TerrainWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + 8 * dy);
        setYRotation(yRot + 8 * dx);
    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + 8 * dy);
        setZRotation(zRot + 8 * dx);
    }
    lastPos = event->pos();
}


static void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
}

void TerrainWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != xRot) {
        xRot = angle;
        updateGL();
    }
}

void TerrainWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != yRot) {
        yRot = angle;
        updateGL();
    }
}

void TerrainWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);
    if (angle != zRot) {
        zRot = angle;
        updateGL();
    }
}

void TerrainWidget::divide()
{
    std::vector<float> neu;
    int size_neu = 2*size;
    neu.resize((size_neu+1)*(size_neu+1));
    for (int y = 0; y < size_neu+1; y++) {
        for (int x = 0; x < size_neu+1; x++) {
            if(x%2 && !(y%2)){
                 neu[y*(size_neu+1)+x] = (heightMap[y/2*(size+1)+x/2]+heightMap[y/2*(size+1)+x/2+1]) * 0.5f +((float)rand()/(float)RAND_MAX*2.0f-1.0f)*factor;
            }else if(!(x%2) && y%2){
                 neu[y*(size_neu+1)+x] = (heightMap[y/2*(size+1)+x/2]+heightMap[(y/2+1)*(size+1)+x/2]) * 0.5f +((float)rand()/(float)RAND_MAX*2.0f-1.0f)*factor;
            }else if(x%2 && y%2){
                 neu[y*(size_neu+1)+x] = (heightMap[y/2*(size+1)+x/2+1]+heightMap[(y/2+1)*(size+1)+x/2]) * 0.5f +((float)rand()/(float)RAND_MAX*2.0f-1.0f)*factor*sqrtf(2.0f);
                 //heightMap[y*(size+1)+x] = (float)rand()/(float)RAND_MAX*factor;
            }else{
                neu[y*(size_neu+1)+x] = heightMap[y/2*(size+1)+x/2];
            }
        }
    }
    heightMap = std::vector<float>(neu);
    size = size_neu;
    factor /= 2.0f;

    this->parentWidget()->parentWidget()->setWindowTitle("Computergrafik - #14 Terrain - "+QString::number((size)*(size)*2)+" Dreiecke");
    generateNormals();
    updateGL();
}


void TerrainWidget::reset()
{
    srand(time(NULL));
    factor = 25.0f;
    size = 1;
    heightMap.resize((size+1)*(size+1));
    for (int y = 0; y < size+1; y++) {
        for (int x = 0; x < size+1; x++) {
            heightMap[y*(size+1)+x] = (float)rand()/(float)RAND_MAX*factor*0.5f-0.5f*factor;
        }
    }
    factor /= 2.0f;

    this->parentWidget()->parentWidget()->setWindowTitle("Computergrafik - #14 Terrain - "+QString::number((size)*(size)*2)+" Dreiecke");
    generateNormals();
    updateGL();
}



void TerrainWidget::generateNormals()
{
    normalMap.clear();

    for (int y = 0; y < size+1; y++) {
        for (int x = 0; x < size+1; x++) {
            normalMap.push_back(GetNormal(QVector2D(x,y)));
        }
    }
}

template <typename _T> inline _T mix(const _T& a,const _T& b, const _T& factor) { return a + factor * (b-a); }

float TerrainWidget::GetInterpolatedHeight(const QVector2D& uv) const {
        // Umbauen, scale erst am ende
        double x,y;
        float sx = modf(uv.x(),&x);
        float sy = modf(uv.y(),&y);

        if ( fabs(sx)<FLT_EPSILON && fabs(sy)<FLT_EPSILON )
                return heightMap[((int)y)*(size+1)+((int)x)];//getElevation(conversion::FloatToInt(x),conversion::FloatToInt(y));

        float h[4];
        h[0] = heightMap[((int)y)*(size+1)+((int)x)];//getElevation((int)x,(int)y);
        h[1] = heightMap[((int)y+1)*(size+1)+((int)x)];//getElevation((int)x+1,(int)y);
        h[2] = heightMap[((int)y)*(size+1)+((int)x+1)];//getElevation((int)x,(int)y+1);
        h[3] = heightMap[((int)y+1)*(size+1)+((int)x+1)];//getElevation((int)x+1,(int)y+1);

        float x0 = mix(h[0],h[1],sx);
        float x1 = mix(h[2],h[3],sx);
        float final = mix(x0,x1,sy);
        return final;
}

QVector3D TerrainWidget::GetNormal(const QVector2D& uv) const {
        float h[4];
        h[0] = GetInterpolatedHeight( uv + QVector2D(-1, 0) );
        h[1] = GetInterpolatedHeight( uv + QVector2D( 1, 0) );
        h[2] = GetInterpolatedHeight( uv + QVector2D( 0,-1) );
        h[3] = GetInterpolatedHeight( uv + QVector2D( 0, 1) );
        QVector3D n;
        n.setX(h[0] - h[1]);
        n.setY(h[2] - h[3]);
        n.setZ(2);
        n.normalize();
        return n;
}

void TerrainWidget::drawVertex(int x, int y) const {
    glNormal3f(normalMap[y*(size+1)+x].x(),normalMap[y*(size+1)+x].y(),normalMap[y*(size+1)+x].z());
    //glVertex3f((x/(float)(size) - 0.5f)*50.0f, (y/(float)(size) - 0.5f)*50.0f, heightMap[y*(size+1)+x]);
    glVertex3f(x, y, heightMap[y*(size+1)+x]);
}

void TerrainWidget::wheelEvent(QWheelEvent *event)
{
    if (event->orientation() == Qt::Vertical) {

        int numDegrees = event->delta() / 8;
        int numSteps = numDegrees / 15;
        distance -= (float) numSteps*0.1f;
        updateGL();
//        qDebug("Vertikal %i", numSteps);
    }
    event->accept();
}

void TerrainWidget::setWireFrame(bool checked) {
    wireframe = checked;
    updateGL();
}
