package MalProgram;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	JMenuBar jmb = new JMenuBar();
	JMenu menu1 = new JMenu("Datei"),
	 menu2 = new JMenu("Einstellungen"),
	 menu3 = new JMenu("Zeichnen"),
	 menu4 = new JMenu("Info");
	
	JMenuItem item11 = new JMenuItem("�ffnen"),
	 item12 = new JMenuItem("Speichern"),
	 item13 = new JMenuItem("Beenden"),
	 item21 = new JMenuItem("Zeichnung l�schen"),
	 item22 = new JMenuItem("Stiftfarbe..."),
	 item23 = new JMenuItem("F�llfarbe...");
	
	JCheckBoxMenuItem item24 = new JCheckBoxMenuItem("F�llen", false);
	 
	JRadioButtonMenuItem[] item3 = new JRadioButtonMenuItem[4];
	String[] action = {"Punkt", "Linie", "Rechteck", "Kreis"};
	ButtonGroup bg = new ButtonGroup();
	JMenuItem item41 = new JMenuItem("Info");
	
	PaintPanel pPanel = new PaintPanel();
	Container cp;
	
	public MainFrame() {
		super("MalProgramm");

		menu1.add(item11);
		menu1.add(item12);
		menu1.addSeparator();
		menu1.add(item13);
		jmb.add(menu1);

		menu2.add(item21);
		menu2.addSeparator();
		menu2.add(item22);
		menu2.add(item23);
		menu2.addSeparator();
		menu2.add(item24);
		jmb.add(menu2);
		
		for (int i = 0; i < action.length; i++) {
			item3[i] = new JRadioButtonMenuItem(action[i]);
			menu3.add(item3[i]);
			bg.add(item3[i]);
		}
		
		item3[0].setSelected(true);
		jmb.add(menu3);
		
		menu4.add(item41);
		jmb.add(menu4);
		
		this.setJMenuBar(jmb);	
		
		add(pPanel);
		
		
		item13.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		item22.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pPanel.setpColor(colorDialog("Zeichenfarbe",Color.BLACK));
			}
		});

		for (int i = 0; i < action.length; i++) {
			item3[i].addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					setPaintMode();
				}
			});
		}
				
		
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);		
		setVisible(true);		
	}
	
	public void setPaintMode() {
		for (int i = 0; i < action.length; i++) {
			if( item3[i].isSelected() )
				pPanel.setpMode(i);
		}
	}
	
	public Color colorDialog(String text, Color farbe) {
		return JColorChooser.showDialog(this,text+" w�hlen", farbe);
	}
	
	public static void main(String[] args) {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		    	System.out.println(info.getName());
		        if ("Nimbus".equals(info.getName())) {
		            //UIManager.setLookAndFeel(info.getClassName());
		            //break;
		        }
		    }
		    
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}

		
		new MainFrame();
	}

}
