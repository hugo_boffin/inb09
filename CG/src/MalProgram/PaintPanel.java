package MalProgram;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JPanel;


public class PaintPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private Color pColor = Color.BLACK;
	private Color fColor = Color.LIGHT_GRAY;
	private boolean fillMode = true;
	private int pMode = 0;
	
	private MyLine actLine = new MyLine();
	private Collection<MyLine> lines = new ArrayList<MyLine>();
	
	public PaintPanel() {
			this.addMouseListener( new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					m_released(e);
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					m_pressed(e);
				}

				@Override
				public void mouseClicked(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}
			}); 
				
			this.addMouseMotionListener(new MouseMotionListener() {
				
				@Override
				public void mouseMoved(MouseEvent e) {
				}
				
				@Override
				public void mouseDragged(MouseEvent e) {
					m_dragged(e);
				}
			});
	}
	
	public Color getpColor() {
		return pColor;
	}

	public void setpColor(Color pColor) {
		this.pColor = pColor;
	}

	public Color getfColor() {
		return fColor;
	}

	public void setfColor(Color fColor) {
		this.fColor = fColor;
	}

	public boolean isFillMode() {
		return fillMode;
	}

	public void setFillMode(boolean fillMode) {
		this.fillMode = fillMode;
	}

	public void setpMode(int pMode) {
		this.pMode = pMode;
	}	
	
	public int getpMode() {
		return pMode;
	}
	

	protected void m_dragged(MouseEvent e) {
		//System.out.println("dragged");
		switch (pMode) {
		case 0:	break;
		case 1:	actLine.setP2(new Point(e.getX(),e.getY())); break;
		case 2:	break;
		case 3:	break;
		}
		this.repaint();
	}

	protected void m_pressed(MouseEvent e) {
		//System.out.println("pressed");
		switch (pMode) {
		case 0:	break;
		case 1:	actLine = new MyLine(new Point(e.getX(),e.getY()),new Point(e.getX(),e.getY()),pColor ); break;
		case 2:	break;
		case 3:	break;
		}
		this.repaint();
	}

	protected void m_released(MouseEvent e) {
		//System.out.println("released");
		switch (pMode) {
		case 0:	break;
		case 1:	actLine.setP2(new Point(e.getX(),e.getY()));lines.add(actLine);actLine = null; break;
		case 2:	break;
		case 3:	break;
		}
		this.repaint();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		switch (pMode) {
		case 0:	break;
		case 1:	if (actLine != null) actLine.painting(g2d, true); break;
		case 2:	break;
		case 3:	break;
		}
		
		for (Iterator<MyLine> line = lines.iterator(); line.hasNext(); )
	        line.next().painting(g2d, true);

	}
}
