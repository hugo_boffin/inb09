package MalProgram;
import java.awt.Graphics2D;
import java.io.Serializable;


public interface Paintable extends Serializable {
	public abstract void painting(Graphics2D g2D, boolean temp);
}
