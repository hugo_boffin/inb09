package MalProgram;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class MyLine implements Paintable {
	private static final long serialVersionUID = 1L;
	
	protected Point p1;
	protected Point p2;
	protected Color col;
		
	public MyLine(Point p1, Point p2, Color col) {
		this.p1 = p1;
		this.p2 = p2;
		this.col = col;
	}
	
	public MyLine() {
		p1 = new Point(0,0);
		p2 = new Point(0,0);
		col = Color.BLUE;
	}
	
	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

	public Color getCol() {
		return col;
	}

	public void setCol(Color col) {
		this.col = col;
	}

	@Override
	public void painting(Graphics2D g2d, boolean temp) {
		g2d.setPaint(col);
		g2d.drawLine(p1.x, p1.y, p2.x, p2.y);
	}

}
