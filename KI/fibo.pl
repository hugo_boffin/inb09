fibo(0,0) :- !.
fibo(1,1) :- !.
fibo(N,F) :- N1 is N-1,
			 fibo(N1,F1),
			 N2 is N1-1,
			 fibo(N2, F2),
			 F is F1 + F2.
			 
fiboliste(0,[0]) :- !.
fiboliste(1,[1,0]) :- !.
fiboliste(N,[X|R]) :- fibo(N,X),N1 is N-1,fiboliste(N1,R).
