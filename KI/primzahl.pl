
keineprimzahl(Z) :- echterTeiler(T, Z).
echterTeiler(T,Z) :- T > 1, T < Z, 0 is Z mod T.
primzahl(Z) :- keineprimzahl(Z),!,fail.
primzahl(Z) :- Z > 1.
