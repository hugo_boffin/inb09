hat_fluegel(X) :- ist_vogel(X).
kann_fliegen(X):- hat_fluegel(X), \+nichtflieger(X).

ist_vogel(meise).
ist_vogel(pinguin).
ist_vogel(amsel).
ist_vogel(strauss).
nichtflieger(pinguin).

