l :- ['~/prolog/prog1.pl'].

%member(X, [X|R]).
%member(X, [Y|R]) :- member(X,R).

subset([X|L1], L2) :- member(X, L2),subset(L1, L2).
subset([],L).

% Letztes Element der Liste ausgeben
% last(X,L) = true <-> X ist letztes El. von L
%last(X,[X]).
%last(X,[_|R]) :- last(X,R).

% 2 Zahlen addieren
sum([], 0).
sum([X|L],S) :- sum(L, S1),S is S1+X.

fak(0, 1).
fak(N, F) :- N1 is N-1, fak(N1, F1), F is F1 * N.

%append([],L, L).
%append([X|L1],L2,[X|L3]) :- append(L1,L2,L3).

%append([1,2],[3,4,5],L).
%append(L,[3,4,5],[1,2,3,4,5]).
%append([1,2],L,[1,2,3,4,5]).
%append(_,[V,3,N|_], [1,2,3,4,5]).
%append(X,Y, [1,2,3,4,5]).

%delete(X,L1,L2) <-> 1. Vorkommen von X auf L1 wird gelöscht, L2 Ergebnis (3 Zeiler)

%delete(X,[],[]).
%delete(X,[X|R],R).
%delete(X,[Y|R1],[Y|R2]) :- delete(X, R1, R2).

% delete(X,[1,2,3],L).
% delete(2,[1,2,3,4,2,5],L).

%deleteall(X,L1,L2) :- member(X,L1), delete(X,L1,L), deleteall(X,L,L2).
%deleteall(X,L,L).

% deleteall(2,[1,2,3,4,2,5],L).
% deleteall(2,[3,2,2,1,2,3,4,5,3,2,3,2],L).

% SlowSort(Liste, Geordnet)

slowsort(L, Sort) :- tausch(L,L1), slowsort(L1, Sort).
slowsort(L, L).
tausch([A,B|L], [B,A|L]) :- A > B.
tausch([X|L1],[X|L2]) :- tausch(L1,L2).

writelist([X|L]) :- write(X), writelist(L).
writelist([]).

% slowsort( [11,10,9,8,7,6,5,4,3,2,1] ,X).
