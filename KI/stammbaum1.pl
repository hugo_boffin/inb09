% Stammbaum in PROLOG

% FAKTEN:
%elter(elter, kind).
elter(della, tick).
elter(della, trick).
elter(della, track).
elter(dortel, della).
elter(dortel, donald).
elter(degenhard, della).
elter(degenhard, donald).
elter(dankrade, mathilda).
elter(dankrade, dagobert).
elter(dankrade, dortel).
elter(dietbert, mathilda).
elter(dietbert, dagobert).
elter(dietbert, dortel).
elter(gustel, dietbert).
elter(gustel, jakob).
elter(gustel, diethelm).
elter(unbekannt, david).
elter(unbekannt, gustel).
elter(minchen, dietbert).
elter(minchen, jakob).
elter(minchen, diethelm).
elter(golo, gustav).
elter(daphne, gustav).
elter(gretchen, dussel).
elter(teddy, dussel).
elter(gretchen, wastel).
elter(teddy, wastel).
elter(hilmar, golo).
elter(hilmar, daphne).
elter(hilmar, teddy).
elter(hilmar, degenhard).
elter(dorette, golo).
elter(dorette, daphne).
elter(dorette, teddy).
elter(dorette, degenhard).
elter(wilhelmine, franz).
elter(gangolf, franz).
elter(emanuel, wilhelmine).
elter(emanuel, willibald).
elter(wilberta, wilhelmine).
elter(wilberta, willibald).
elter(gunhilda, dorette).
elter(gunhilda, emanuel).
elter(emelrich, dorette).
elter(emelrich, emanuel).

% REGELN:

% kindvon(Kind, Elternteil).
kindvon(A,B) :- elter(B,A).

verbunden(A,B) :- elter(A,B).
verbunden(A,B) :- kindvon(A,B).

% grad(Startknoten, Zielknoten, Verwandschaftsgrad).
grad(Start,Ziel,Grad) :- grad(Ziel,[Start],[],[],0,Grad).

% grad(Ziel,Warteschlange,WarteschlangeNaechsteTiefe,Bekannt,Grad,Loesung)
grad(_,[],[],_,_,_) :-
	write('Nicht verwandt!'),nl,!,fail.
grad(Ziel,[Ziel|_],_,_,Grad,Grad) :-
	write('Verwandschaftsgrad '),write(Grad),write('!'),nl,!.
grad(Ziel,[],NaechsteTiefe,Bekannt,Grad,Loesung) :-
	NeuerGrad is (Grad+1),
	write('Bekannt: '),writeList(Bekannt),nl,
	grad(Ziel,NaechsteTiefe,[],Bekannt,NeuerGrad,Loesung).
grad(Ziel,[Pivot|R],NaechsteTiefe,Bekannt,Grad,Loesung) :-
	%IF
	setof(Y,(verbunden(Pivot,Y),\+ enthaelt(Y,Bekannt)),NeueKnoten), !,
	%THEN
	write('+: '),writeList(NeueKnoten),nl,
	anfuegen(NaechsteTiefe,NeueKnoten,NeueNaechsteTiefe),
	anfuegen(Bekannt,NeueKnoten,NeuBekannt),
	grad(Ziel,R,NeueNaechsteTiefe,NeuBekannt,Grad,Loesung).
	%ELSE
grad(Ziel,[_|R],NaechsteTiefe,Bekannt,Grad,Loesung) :- 
	grad(Ziel,R,NaechsteTiefe,Bekannt,Grad,Loesung).
	
ersterGrad(K,L) :- setof(Y,verbunden(K,Y),L).

enthaelt(X, [X|_]).
enthaelt(X, [_|R]) :- enthaelt(X,R).

anfuegen([],L, L).
anfuegen([X|L1],L2,[X|L3]) :- anfuegen(L1,L2,L3).
	
% Debugging
writeList([]).
writeList([X|R]) :- write(X),write(' '),writeList(R).