% Stammbaum in PROLOG

% FAKTEN:

% elter(Elternteil, Kind).
elter(balbo,mungo).
elter(berylla,mungo).
elter(mungo,bungo).
elter(laura,bungo).
elter(bungo,bilbo).
elter(belladonna,bilbo).
elter(mungo,belba).
elter(laura,belba).
elter(mungo,longo).
elter(laura,longo).
elter(longo,otho).
elter(camellia,otho).
elter(otho,lotho).
elter(lobelia,lotho).
elter(mungo,linda).
elter(laura,linda).
elter(bodo,odo).
elter(linda,odo).
elter(odo,olo).
elter(olo,sancho).
elter(mungo,bingo).
elter(laura,bingo).
elter(chica,falco).
elter(bingo,falco).
elter(falco,magsame).
elter(balbo,viola).
elter(berylla,viola).
elter(balbo,ponto).
elter(berylla,ponto).
elter(ponto,rosa).
elter(mimosa,rosa).
% Peregrin Meriadoc ...
elter(ponto,polo).
elter(mimosa,polo).
elter(polo,posco).
elter(posco,pontoJr).
elter(nelke,pontoJr).
elter(pontoJr,angelica).
elter(posco,porto).
elter(nelke,porto).
elter(posco,paeonie).
elter(nelke,paeonie).
elter(paeonie,mosco).
elter(milo,mosco).
elter(paeonie,moro).
elter(milo,moro).
elter(paeonie,myrte).
elter(milo,myrte).
elter(paeonie,minto).
elter(milo,minto).
elter(polo,prisca).
elter(balbo,largo).
elter(berylla,largo).
elter(largo,fosco).
elter(tanta,fosco).
elter(fosco,dora).
elter(rubinia,dora).
elter(fosco,drogo).
elter(rubinia,drogo).
elter(drogo,frodo).
elter(primula,frodo).
elter(fosco,dudo).
elter(rubinia,dudo).
elter(dudo,margerite).
elter(balbo,lily).
elter(berylla,lily).
% verschiedene Gutleibs...

% Die Eheschlie�ung begr�ndet keine Verwandtschaft der Ehepartner. (Wikipedia)
% verheiratet(A, B).
/*
verheiratet(balbo,berylla).
verheiratet(mungo,laura).
verheiratet(bungo,belladonna).
verheiratet(belba,rudigar).
verheiratet(longo,camellia).
verheiratet(otho,lobelia).
verheiratet(linda,bodo).
verheiratet(bingo,chica).
verheiratet(magsame,filibert).
verheiratet(viola,fastolph).
verheiratet(ponto,mimosa).
verheiratet(rosa,hildigrim).
verheiratet(posco,nelke).
verheiratet(paeonie,milo).
verheiratet(prisca,willibald).
verheiratet(drogo,primula).
verheiratet(margerite,griffo).
verheiratet(fosco,rubinia).
verheiratet(largo,tanta).
verheiratet(lily,togo).
*/

% REGELN:

% kindvon(Kind, Elternteil).
kindvon(A,B) :- elter(B,A).

verbunden(A,B) :- elter(A,B).
verbunden(A,B) :- kindvon(A,B).

% grad(Startknoten, Zielknoten, Verwandschaftsgrad).
grad(Start,Ziel,Grad) :- grad(Ziel,[Start],[],[],0,Grad).

% grad(Ziel,Warteschlange,WarteschlangeNaechsteTiefe,Bekannt,Grad,Loesung)
grad(_,[],[],_,_,_) :-
	write('Nicht verwandt!'),nl,!.
grad(Ziel,[Ziel|_],_,_,Grad,Grad) :-
	write('Verwandschaftsgrad '),write(Grad),write('!'),nl,!.
grad(Ziel,[],NaechsteTiefe,Bekannt,Grad,Loesung) :-
	NeuerGrad is (Grad+1),
	write('Bekannt: '),writeList(Bekannt),nl,
	grad(Ziel,NaechsteTiefe,[],Bekannt,NeuerGrad,Loesung).
grad(Ziel,[Pivot|R],NaechsteTiefe,Bekannt,Grad,Loesung) :-
	%IF
	setof(Y,(verbunden(Pivot,Y),\+ enthaelt(Y,Bekannt)),NeueKnoten), !,
	%THEN
	write('+: '),writeList(NeueKnoten),nl,
	anfuegen(NaechsteTiefe,NeueKnoten,NeueNaechsteTiefe),
	anfuegen(Bekannt,NeueKnoten,NeuBekannt),
	grad(Ziel,R,NeueNaechsteTiefe,NeuBekannt,Grad,Loesung).
	%ELSE
grad(Ziel,[_|R],NaechsteTiefe,Bekannt,Grad,Loesung) :- 
	grad(Ziel,R,NaechsteTiefe,Bekannt,Grad,Loesung).
	
ersterGrad(K,L) :- setof(Y,verbunden(K,Y),L).

enthaelt(X, [X|_]).
enthaelt(X, [_|R]) :- enthaelt(X,R).

anfuegen([],L, L).
anfuegen([X|L1],L2,[X|L3]) :- anfuegen(L1,L2,L3).
	
% Debugging
writeList([]).
writeList([X|R]) :- write(X),write(' '),writeList(R).
