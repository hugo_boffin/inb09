% X element R ?
member(X, [X|_]).
member(X, [_|R]) :- member(X,R).

% tag(Tag,Monat,Jahr) - Gueltiges Datum ?
tag(Tag,Monat,_Jahr) :- member(Monat,[1,3,5,7,8,10,12]), 1 =< Tag, Tag =< 31.
tag(Tag,Monat,_Jahr) :- member(Monat,[4,6,9,11]), 1 =< Tag, Tag =< 30.
tag(Tag,2,Jahr) :- Jahr mod 4 =:= 0, Jahr mod 400 =\= 0, 1 =< Tag, Tag =< 29, !.
tag(Tag,2,_Jahr) :- 1 =< Tag, Tag =< 28.

